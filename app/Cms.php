<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Spatie\Activitylog\Traits\LogsActivity;

class Cms extends Model
{
    //use LogsActivity;
    protected $table = 'cms_content';
    protected $fillable =[
        "trader_id", "about", "contact","terms","privacy"
   ];
    protected static $logAttributes=[
        "trader_id", "about", "contact","terms","privacy"
   ];

    // public function product()
    // {
    // 	//return $this->hasMany('App\Product');

    // }
}
