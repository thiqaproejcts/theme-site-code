<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Notifications\Notifiable;
//use Illuminate\Foundation\Auth\User as Authenticatable;
//use Spatie\Permission\Traits\HasRoles;
//use Spatie\Activitylog\Traits\LogsActivity;

class SiteConfiguration extends Model
{
    // use Notifiable;
    // use HasRoles;
//    use LogsActivity;
protected $table = 'site_configuration';
    protected $fillable = [
        "trader_id", "website_name", "domain", "phone", "email", "facebook_page", "instagram_page", "twitter_page", "google_analytics_code", "seo_title", "seo_decription", "slider_img", "slider_title", "theme1_img", "theme2_img", "theme3_img", "token"
    ];
    protected static $logAttributes= [
       "trader_id", "website_name", "domain", "phone", "email", "facebook_page", "instagram_page", "twitter_page", "google_analytics_code", "seo_title", "seo_decription", "slider_img", "slider_title", "theme1_img", "theme2_img", "theme3_img", "token"
    ];
    // protected $fillable = [
    //     'name',
    //     'email',
    //     'password',
    // ];
    // protected $hidden = [
    //     'password', 'remember_token',
    // ];

    // public function isActive()
    // {
    //     return $this->is_active;
    // }

    // public function holiday() {
    //     return $this->hasMany('App\Holiday');
    // }
}
