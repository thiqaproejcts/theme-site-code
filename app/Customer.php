<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Spatie\Activitylog\Traits\LogsActivity;

class Customer extends Model
{
   // use LogsActivity;
    protected $fillable =[
        "customer_group_id", "user_id", "name", "company_name",
        "email", "phone_number", "tax_no", "address", "city",
        "state", "postal_code","trader_id","wilaya_id","region_id","country","is_deleted", "deposit", "expense", "is_active","created_by","code"
    ];
    // protected static $logAttributes =[
    //     "customer_group_id", "user_id", "name", "company_name",
    //     "email", "phone_number", "tax_no", "address", "city",
    //     "state", "postal_code","trader_id","wilaya_id","region_id","country", "deposit", "expense", "is_active","created_by"
    // ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
