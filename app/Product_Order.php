<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Spatie\Activitylog\Traits\LogsActivity;

class Product_Order extends Model
{
    //use LogsActivity;
	protected $table = 'product_orders';
    protected $fillable =[
        "sale_id", "product_id", "variant_id", "qty", "sale_unit_id", "net_unit_price", "discount", "tax_rate","total_weight", "tax", "total","trader_id","total_weight"
    ];
    // protected static $logAttributes=[
    //     "sale_id", "product_id", "variant_id", "qty", "sale_unit_id", "net_unit_price", "discount", "tax_rate","total_weight", "tax", "total","trader_id","total_weight"
    // ];
}
