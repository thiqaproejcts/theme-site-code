<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Spatie\Activitylog\Traits\LogsActivity;

class CmsSlider extends Model
{
    //use LogsActivity;
    protected $table = 'cms_sliders';
    protected $fillable =[
         "trader_id", "slider_img", "show","type","update_at","created_at"
   ];
    protected static $logAttributes=[
         "trader_id", "slider_img", "show","type","update_at","created_at"
   ];

    // public function product()
    // {
    // 	//return $this->hasMany('App\Product');

    // }
}
