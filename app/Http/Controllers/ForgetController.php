<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Product;
use App\SiteConfiguration;
use App\Cms;
use App\CmsSlider;
use App\Customer;
class ForgetController extends Controller
{
    public function forgetPassword()
    {
        session_start();
        $token="o7P71HKBGfhaRtDd5R15sZNsOv6rNtve1dHRQlXV";
        $check=DB::select(" SELECT * FROM `site_configuration` WHERE `token`='".$token."' ");
        //dd($check);
        foreach($check as $cc)
        {
               $trader_id=$cc->trader_id;
               $theme=$cc->theme;
               $phone=$cc->phone;
               $email=$cc->email;
               $logo=$cc->logo_img;
               
               $_SESSION["trader_id"]=$trader_id;
               $_SESSION["phone"]=$phone;
               $_SESSION["email"]=$email;
               $_SESSION["theme"]=$theme;
                $_SESSION["logo"]=$logo;
        }
        return view("pages.forget");
    }
    public function sixDigit(Request $request)
    {
        session_start();
        $token="o7P71HKBGfhaRtDd5R15sZNsOv6rNtve1dHRQlXV";
        $check1=DB::select(" SELECT * FROM `site_configuration` WHERE `token`='".$token."' ");
        //dd($check);
        foreach($check1 as $cc)
        {
               $trader_id=$cc->trader_id;
               $theme=$cc->theme;
               $phone=$cc->phone;
               $email=$cc->email;
               $logo=$cc->logo_img;
               
               $_SESSION["trader_id"]=$trader_id;
               $_SESSION["phone"]=$phone;
               $_SESSION["email"]=$email;
               $_SESSION["theme"]=$theme;
                $_SESSION["logo"]=$logo;
        }

        $check=Customer::where("email",$request->email)->get();
        if($check->count()>0)
        {
            foreach($check as $c)
            {
                $id=$c->id;
            }
            $six_digit_random_number = random_int(100000, 999999);
            $to = $request->email;
$subject = "Six digit code";

$message = 'Your six digit code is='.$six_digit_random_number.',<br>';

// More headers
$headers = 'From: <admin@thiqatech.com>' . "\r\n";

mail($to,$subject,$message,$headers);
DB::select(" UPDATE `customers` SET `code`='".$six_digit_random_number."' WHERE `id`='".$id."' ");
            return view("pages.six")->with("id",$id);
        }
        else
        {
            return redirect()->action("ForgetController@forgetPassword")->with("message","The provided email is not registered in the record.");
        }



        //return view("pages.six");
    }
    public function checksixdigit(Request $request)
    {
        session_start();
        $token="o7P71HKBGfhaRtDd5R15sZNsOv6rNtve1dHRQlXV";
        $check1=DB::select(" SELECT * FROM `site_configuration` WHERE `token`='".$token."' ");
        //dd($check);
        foreach($check1 as $cc)
        {
               $trader_id=$cc->trader_id;
               $theme=$cc->theme;
               $phone=$cc->phone;
               $email=$cc->email;
               $logo=$cc->logo_img;
               
               $_SESSION["trader_id"]=$trader_id;
               $_SESSION["phone"]=$phone;
               $_SESSION["email"]=$email;
               $_SESSION["theme"]=$theme;
                $_SESSION["logo"]=$logo;
        }
        $code=$request->six;
        $check=Customer::where("code",$code)->get();
        if($check->count()>0)
        {
            foreach($check as $c)
            {
                $id=$c->id;
            }
            return view("pages.set_password")->with("id",$id);
        }
        else
        {
            return redirect()->action("ForgetController@sixDigit")->with("message","Your entered six digit code do not match");
        }
        
    }
    public function setPassword(Request $request)
    {
        $id=$request->id;
        $password=$request->password;
        $retype=$request->retypepassword;

    }
    
}