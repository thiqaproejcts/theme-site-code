<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Product;
use App\SiteConfiguration;
use App\Cms;
use App\CmsSlider;
use App\Customer;
use App\Order;
use App\Product_Order;
class CheckoutController extends Controller
{

public function checkoutfinal(Request $request)
    {
        session_start();
        //dd($request);
        
        $firstname=$request->firstname;
        $lastname=$request->lastname;
        $postcode=$request->postcode;
        $address=$request->address;
        $ordernote=$request->ordernote;
        $payment=$request->payment;
        $country=$request->country;
        $town=$request->town;
        $company=$request->company;
        $phone=$request->phone;

        $total_price=0;
        $total_qty=0;
        $total_weight=1;
        $count=0;


        if($payment=="thawani")
        {
            
$data['client_reference_id'] = '';
$data['mode'] = 'payment';
$item = array();
foreach(session('cart') as $id => $details)
{
    array_push(
        $item,array(
            "name"=>$details['name'],
            "quantity"=>$details['quantity'],
            "unit_amount"=>($details['price']*1000)
            )
        );
}

//$products[] = $item;
//dd($products);
$data['products'] = json_decode(json_encode($item));
//return $data["products"];
$data['customer_id'] = '';
$data['success_url'] = 'https://rahma.thiqatech.com/public/thawani/success/' . $request->user_id . '/' . $request->total_amount . '/' . $request->lang;
$data['cancel_url'] = 'https://rahma.thiqatech.com/public/thawani/success/cancel';
$data['save_card_on_success'] = false;
$curl = curl_init('https://checkout.thawani.om/api/v1/checkout/session');
curl_setopt($curl, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Connection: Keep-Alive',
    'thawani-api-key: pvPEuUGRDpJnRB7tzdE33BCj3aU1Qj'
));
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode(($data)));
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($curl);
curl_close($curl);
$json = json_decode($response);
//return  json_encode($json);
if ($json->success)
    return redirect()->to('https://checkout.thawani.om/pay/' . $json->data->session_id . '?key=LT3fHOOnuh1B8XWAZuMDyQbPfMojy7 ');
else
    return redirect()->to('https://perfumes.fasttech.om/perfume/public/api/payment_failed');
        }

        if(session('cart'))
        {
        foreach(session('cart') as $id => $details)
        {
            $total_price+=($details["price"]*$details["quantity"]);
            $total_qty+=$details["quantity"];
            $count++;
        }
    }
$customer_name=$firstname." ".$lastname;
        $data['reference_no'] = 'ts-' . date("Ymd") . '-'. date("his");

        $today=date("Y-m-d");
        $curl = curl_init();
     $total_price+=2;
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://api.dalileeom.com/test/public/api/v1/addordertasheel',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => array('branchid' => '1','quantity' => $total_qty,'weight' => $total_weight,'region_id' => "1",'order_date' =>$today,'doctype' => '0','customer_name' =>$customer_name,'customer_number' => $phone,'order_price' => $total_price,'wallaya_id' => "1",'type' => '')
        ));

        
        
        
        $response = curl_exec($curl);
        
        curl_close($curl);
       //dd($response);
        $someObject = json_decode("[$response]");
        $dalileonumber=$someObject[0]->data;
            $data['dalilee_order_id'] = $dalileonumber;
            $data["user_id"]=$_SESSION["userid"];
            $data["cash_register_id"]="";
            $data["customer_id"]=$_SESSION["userid"];
            $data["warehouse_id"]="";
            $data["biller_id"]="";
            $data["item"]=$count;
            $data["total_qty"]=$total_qty;
            $data["order_weight"]=0;
            $data["total_discount"]=0;
            $data["total_tax"]=0;
            $data['payment_status']=0;
            $data["total_price"]=($total_price-2);
            $data["grand_total"]=$total_price;
            $data['shipping_cost']=2;
            $data['sale_note']=$ordernote;
            $data['wilaya_id']="";
            $data['region_id']="";
            $data['trader_id'] = $_SESSION['trader_id'];
            $data['order_tax_rate']="";
            $data['order_tax']="";
            $data['order_discount']="";

            $data['paid_amount']="";
            $data['document']="";
            $data['order_status']="";
            $data['sale_status']=0;
            $data['coupon_discount']="";
            $data['coupon_id']="";
            $data["is_delivery"]=0;
            $data["service_charges"]="";
            $data["collection_id"]="";

            $data["p_invoice"]="";
            $data["a_invoice"]="";

            $data["created_at"]=$today;
            $data["updated_at"]=$today;
            $data["client_order"]="";
            $data["discount_type"]="";
            $data["extra_fees"]="";
            $data["is_closed"]="";
            $data["is_confirmed"]="";
            $data["closed_date"]="";
            $data["invoice_no"]="";
            $data["store_id"]="";
            $data["store_name"]="";
            $lims_sale_data = Order::create($data);

            foreach(session('cart') as $id => $details)
        {
            $pdata["sale_id"]=$lims_sale_data->id;
            $pdata["product_id"]=$details["itemcode"];
            $pdata["variant_id"]="";
            $pdata["qty"]=$details["quantity"];
            $pdata["created_at"]=$today;
            $pdata["total_weight"]=0;
            $pdata["trader_id"]=$_SESSION["userid"];
            $pdata["total"]=($details["quantity"]*$details["price"]);
            $pdata["tax"]=0;
            $pdata["tax_rate"]=0;
            $pdata["discount"]=0;
            $pdata["net_unit_price"]=$details["price"];
            $pdata["sale_unit_id"]=1;
            $pdata["updated_at"]=$today;
            $itemcode=$details["itemcode"];
          Product_Order::create($pdata);
          $cart = session()->get('cart');
          //dd($cart[$id]);
          unset($cart[$itemcode]);
          session()->put('cart', $cart);
        }
return redirect()->action("HomeController@success_page");
             }

             public function checkoutfinalcheck()
             {

             }

}