<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Product;
use App\SiteConfiguration;
use App\Cms;
use App\CmsSlider;
use App\Customer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
class ProfileController extends Controller
{
    public function profile(Request $request)
    {
        session_start();
        //session()->forget('cart');
         $token="o7P71HKBGfhaRtDd5R15sZNsOv6rNtve1dHRQlXV";
         $check=DB::select(" SELECT * FROM `site_configuration` WHERE `token`='".$token."' ");
         //dd($check);
         foreach($check as $cc)
         {
                $trader_id=$cc->trader_id;
                $theme=$cc->theme;
                $phone=$cc->phone;
                $email=$cc->email;
                $logo=$cc->logo_img;
                
                $_SESSION["trader_id"]=$trader_id;
                $_SESSION["phone"]=$phone;
                $_SESSION["email"]=$email;
                $_SESSION["theme"]=$theme;
                 $_SESSION["logo"]=$logo;
               
                
         }
         $customer=Customer::where("id",$_SESSION["userid"])->get();
     return view("pages.profile")->with("customer",$customer);
    }

    public function updateprofile(Request $request)
    {
        session_start();
        $email=$request->email;
        $password=$request->password;
        $name=$request->name;
        $phone=$request->phone;
        $address=$request->address;
        $company=$request->company;
        $city=$request->city;
        $postal=$request->postal;
        $country=$request->country;
       
     if($password!="")
        {
            $hash=hash('gost',$password); 
        DB::select(" UPDATE `customers` SET `name`='".$name."',`company_name`='".$company."',`email`='".$email."',`phone_number`='".$phone."',`address`='".$address."',`city`='".$city."',`postal_code`='".$postal."',`country`='".$country."',`password`='".$hash."' WHERE `id`='".$_SESSION["userid"]."' ");
    }
    else
    {
        DB::select(" UPDATE `customers` SET `name`='".$name."',`company_name`='".$company."',`email`='".$email."',`phone_number`='".$phone."',`address`='".$address."',`city`='".$city."',`postal_code`='".$postal."',`country`='".$country."' WHERE `id`='".$_SESSION["userid"]."' ");
    }
        return redirect()->action("ProfileController@profile")->with("success","Profile Updated.");
    }
  
}