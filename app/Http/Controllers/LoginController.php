<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Product;
use App\SiteConfiguration;
use App\Cms;
use App\CmsSlider;
use App\Customer;
use Illuminate\Support\Facades\Hash;
class LoginController extends Controller
{
    public function login(Request $request)
    {
        session_start();
        $hash=Hash::make($request->password);
        $ch=Customer::where("name",$request->username)->where("password",$hash)->get();
        if($ch->count()>0)
        {
            foreach($ch as $cc)
            {
                $user_id=$cc->id;
            }
            $_SESSION["userid"]=$user_id;
            return redirect()->action("HomeController@index");
        }
        else
        {
            return redirect()->action("HomeController@login")->with("message","Your username or password doesnot match.");
        }
    }
    public function loginpost(Request $request)
    {
        $email=$request->email;
        $password=$request->password;
        //$hash=Hash::make($password);
        //dd(Hash::check('password', Hash::make($password)));
        //dd($hash);
        //$password=$_POST['password'];
        //echo $id;
        /*echo $user;
        echo $password;
        */
        $hash=hash('gost',$password); 
        $check=Customer::where("email",$email)->where("password",$hash)->get();
        if($check->count()>0)
        {
            foreach($check as $cc)
            {
                $id=$cc->id;
            }
            session_start();
            $_SESSION["userid"]=$id;
            return redirect()->action("HomeController@index");
            
        }
        else
        {
            return redirect()->action("HomeController@login")->with("messagel","Wrong email or password");
        }

    }
}