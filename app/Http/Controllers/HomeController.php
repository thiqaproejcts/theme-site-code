<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Product;
use App\SiteConfiguration;
use App\Cms;
use App\CmsSlider;
use App\Order;
use App\Product_Order;
class HomeController extends Controller
{
    public function index()
    {
        session_start();
       //session()->forget('cart');
        $token="xCfUqVMyiyL5iE2mRD78le3XTL7sAHoBh2xpZqwt";
        $check=DB::select(" SELECT * FROM `site_configuration` WHERE `token`='".$token."' ");
        //dd($check);
        foreach($check as $cc)
        {
               $trader_id=$cc->trader_id;
               $theme=$cc->theme;
               $phone=$cc->phone;
               $email=$cc->email;
               $logo=$cc->logo_img;
               
               $_SESSION["trader_id"]=$trader_id;
               $_SESSION["phone"]=$phone;
               $_SESSION["email"]=$email;
               $_SESSION["theme"]=$theme;
                $_SESSION["logo"]=$logo;
              
               
        }
        $slider=CmsSlider::where("trader_id",$_SESSION["trader_id"])->where("type","mainslider")->get();
        $underslider=CmsSlider::where("trader_id",$_SESSION["trader_id"])->where("type","undermainslider")->get();
        $foodbanner=CmsSlider::where("trader_id",$_SESSION["trader_id"])->where("type","foodbanner")->get();
        $mobilebanner=CmsSlider::where("trader_id",$_SESSION["trader_id"])->where("type","mobilebanner")->get();
        $electronicbanner=CmsSlider::where("trader_id",$_SESSION["trader_id"])->where("type","electronicbanner")->get();
        $submainbanner=CmsSlider::where("trader_id",$_SESSION["trader_id"])->where("type","submainbanner")->get();
        //dd($_SESSION["trader_id"]);
        $product=Product::where("trader_id",$trader_id)->where("image","!=","zummXD2dvAtI.png")->limit(5)->get();
        $food=Product::where("trader_id",$trader_id)->where("image","!=","zummXD2dvAtI.png")->limit(4)->get();
        $mobile=Product::where("trader_id",$trader_id)->where("image","!=","zummXD2dvAtI.png")->limit(4)->get();
        
        
        if($_SESSION["theme"]==1)
        {
        return view("home",compact("product","food","mobile","slider","underslider","foodbanner","mobilebanner","electronicbanner","submainbanner"));
        }
        else if($_SESSION["theme"]==2)
        {
            return view("home2",compact("product","food","mobile","slider","underslider","foodbanner","mobilebanner","electronicbanner","submainbanner"));
        }
        else
        {
            return view("home3",compact("product","food","mobile","slider","underslider","foodbanner","mobilebanner","electronicbanner","submainbanner"));
        }
    }
    public function contactus()
    {
        session_start();
        $token="o7P71HKBGfhaRtDd5R15sZNsOv6rNtve1dHRQlXV";
        $check=DB::select(" SELECT * FROM `site_configuration` WHERE `token`='".$token."' ");
        //dd($check);
        foreach($check as $cc)
        {
               $trader_id=$cc->trader_id;
               $theme=$cc->theme;
               $phone=$cc->phone;
               $email=$cc->email;
               $logo=$cc->logo_img;
               
               $_SESSION["trader_id"]=$trader_id;
               $_SESSION["phone"]=$phone;
               $_SESSION["email"]=$email;
               $_SESSION["theme"]=$theme;
                $_SESSION["logo"]=$logo;
        }
        $co=Cms::where("trader_id",$_SESSION["trader_id"])->get();
        foreach($co as $c)
        {
            $contact=$c->contact;
        }
        return view("pages.contact",compact("contact"));
    }
    public function aboutus()
    {
        session_start();
        $token="o7P71HKBGfhaRtDd5R15sZNsOv6rNtve1dHRQlXV";
        $check=DB::select(" SELECT * FROM `site_configuration` WHERE `token`='".$token."' ");
        //dd($check);
        foreach($check as $cc)
        {
               $trader_id=$cc->trader_id;
               $theme=$cc->theme;
               $phone=$cc->phone;
               $email=$cc->email;
               $logo=$cc->logo_img;
               
               $_SESSION["trader_id"]=$trader_id;
               $_SESSION["phone"]=$phone;
               $_SESSION["email"]=$email;
               $_SESSION["theme"]=$theme;
                $_SESSION["logo"]=$logo;
        }
        $co=Cms::where("trader_id",$_SESSION["trader_id"])->get();
        foreach($co as $c)
        {
            $about=$c->about;
        }
        return view("pages.about",compact("about"));
    }
    public function privacy()
    {
        session_start();
        $token="o7P71HKBGfhaRtDd5R15sZNsOv6rNtve1dHRQlXV";
        $check=DB::select(" SELECT * FROM `site_configuration` WHERE `token`='".$token."' ");
        //dd($check);
        foreach($check as $cc)
        {
               $trader_id=$cc->trader_id;
               $theme=$cc->theme;
               $phone=$cc->phone;
               $email=$cc->email;
               $logo=$cc->logo_img;
               
               $_SESSION["trader_id"]=$trader_id;
               $_SESSION["phone"]=$phone;
               $_SESSION["email"]=$email;
               $_SESSION["theme"]=$theme;
                $_SESSION["logo"]=$logo;
        }
        $co=Cms::where("trader_id",$_SESSION["trader_id"])->get();
        foreach($co as $c)
        {
            $privacy=$c->privacy;
        }
        return view("pages.privacy",compact("privacy"));
    }
    public function terms()
    {
        session_start();
        $token="o7P71HKBGfhaRtDd5R15sZNsOv6rNtve1dHRQlXV";
        $check=DB::select(" SELECT * FROM `site_configuration` WHERE `token`='".$token."' ");
        //dd($check);
        foreach($check as $cc)
        {
               $trader_id=$cc->trader_id;
               $theme=$cc->theme;
               $phone=$cc->phone;
               $email=$cc->email;
               $logo=$cc->logo_img;
               
               $_SESSION["trader_id"]=$trader_id;
               $_SESSION["phone"]=$phone;
               $_SESSION["email"]=$email;
               $_SESSION["theme"]=$theme;
                $_SESSION["logo"]=$logo;
        }
        $co=Cms::where("trader_id",$_SESSION["trader_id"])->get();
        foreach($co as $c)
        {
            $terms=$c->terms;
        }
        return view("pages.terms",compact("terms"));
    }

    public function mycart()
    {
        session_start();
        $token="o7P71HKBGfhaRtDd5R15sZNsOv6rNtve1dHRQlXV";
        $check=DB::select(" SELECT * FROM `site_configuration` WHERE `token`='".$token."' ");
        //dd($check);
        foreach($check as $cc)
        {
               $trader_id=$cc->trader_id;
               $theme=$cc->theme;
               $phone=$cc->phone;
               $email=$cc->email;
               $logo=$cc->logo_img;
               
               $_SESSION["trader_id"]=$trader_id;
               $_SESSION["phone"]=$phone;
               $_SESSION["email"]=$email;
               $_SESSION["theme"]=$theme;
                $_SESSION["logo"]=$logo;
        }
        $co=Cms::where("trader_id",$_SESSION["trader_id"])->get();
        foreach($co as $c)
        {
            $terms=$c->terms;
        }
        return view("pages.mycart",compact("terms"));
    }

    public function checkout()
    {
        //session_start();
        // $token="o7P71HKBGfhaRtDd5R15sZNsOv6rNtve1dHRQlXV";
        // $check=DB::select(" SELECT * FROM `site_configuration` WHERE `token`='".$token."' ");
        // //dd($check);
        // foreach($check as $cc)
        // {
        //        $trader_id=$cc->trader_id;
        //        $theme=$cc->theme;
        //        $phone=$cc->phone;
        //        $email=$cc->email;
        //        $logo=$cc->logo_img;
               
        //        $_SESSION["trader_id"]=$trader_id;
        //        $_SESSION["phone"]=$phone;
        //        $_SESSION["email"]=$email;
        //        $_SESSION["theme"]=$theme;
        //         $_SESSION["logo"]=$logo;
        // }
        $co=Cms::where("trader_id",$_SESSION["trader_id"])->get();
        foreach($co as $c)
        {
            $terms=$c->terms;
        }
        return view("pages.checkout",compact("terms"));
    }
    
    public function orderhistory()
    {
        session_start();
        $token="o7P71HKBGfhaRtDd5R15sZNsOv6rNtve1dHRQlXV";
        $check=DB::select(" SELECT * FROM `site_configuration` WHERE `token`='".$token."' ");
        //dd($check);
        foreach($check as $cc)
        {
               $trader_id=$cc->trader_id;
               $theme=$cc->theme;
               $phone=$cc->phone;
               $email=$cc->email;
               $logo=$cc->logo_img;
               
               $_SESSION["trader_id"]=$trader_id;
               $_SESSION["phone"]=$phone;
               $_SESSION["email"]=$email;
               $_SESSION["theme"]=$theme;
                $_SESSION["logo"]=$logo;
        }
        $records=Order::where("sale_status",0)->where("customer_id",$_SESSION["userid"])->get();
        return view("pages.orderhistory",compact("records"));
    }

    public function wishlist()
    {
        session_start();
        $token="o7P71HKBGfhaRtDd5R15sZNsOv6rNtve1dHRQlXV";
        $check=DB::select(" SELECT * FROM `site_configuration` WHERE `token`='".$token."' ");
        //dd($check);
        foreach($check as $cc)
        {
               $trader_id=$cc->trader_id;
               $theme=$cc->theme;
               $phone=$cc->phone;
               $email=$cc->email;
               $logo=$cc->logo_img;
               
               $_SESSION["trader_id"]=$trader_id;
               $_SESSION["phone"]=$phone;
               $_SESSION["email"]=$email;
               $_SESSION["theme"]=$theme;
                $_SESSION["logo"]=$logo;
        }
        $co=Cms::where("trader_id",$_SESSION["trader_id"])->get();
        foreach($co as $c)
        {
            $terms=$c->terms;
        }
        return view("pages.wishlist",compact("terms"));
    }

    public function productdetails($id)
    {
        session_start();
        $token="o7P71HKBGfhaRtDd5R15sZNsOv6rNtve1dHRQlXV";
        $check=DB::select(" SELECT * FROM `site_configuration` WHERE `token`='".$token."' ");
        //dd($check);
        foreach($check as $cc)
        {
               $trader_id=$cc->trader_id;
               $theme=$cc->theme;
               $phone=$cc->phone;
               $email=$cc->email;
               $logo=$cc->logo_img;
               
               $_SESSION["trader_id"]=$trader_id;
               $_SESSION["phone"]=$phone;
               $_SESSION["email"]=$email;
               $_SESSION["theme"]=$theme;
                $_SESSION["logo"]=$logo;
        }
        $product=Product::where("id",$id)->get();
        return view("pages.product_detail",compact("product"));
    }

    public function login()
    {
        session_start();
        $token="o7P71HKBGfhaRtDd5R15sZNsOv6rNtve1dHRQlXV";
        $check=DB::select(" SELECT * FROM `site_configuration` WHERE `token`='".$token."' ");
        //dd($check);
        foreach($check as $cc)
        {
               $trader_id=$cc->trader_id;
               $theme=$cc->theme;
               $phone=$cc->phone;
               $email=$cc->email;
               $logo=$cc->logo_img;
               
               $_SESSION["trader_id"]=$trader_id;
               $_SESSION["phone"]=$phone;
               $_SESSION["email"]=$email;
               $_SESSION["theme"]=$theme;
                $_SESSION["logo"]=$logo;
        }
        return view("pages.login");
    }
    
    public function success_page()
    {
        return view("pages.success");
    }
    public function orderdetails($id)
    {
        session_start();
$records=Product_Order::where("sale_id",$id)
->join('products', 'products.id', '=', 'product_orders.product_id')
->get();
return view("pages.order_details",compact("records"));
    }
    
}
