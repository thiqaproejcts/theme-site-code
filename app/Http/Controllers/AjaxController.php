<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Product;
use App\SiteConfiguration;
use App\Cms;
use App\CmsSlider;
class AjaxController extends Controller
{
    // public function index()
    // {
    //     session_start();
    //     $token="xCfUqVMyiyL5iE2mRD78le3XTL7sAHoBh2xpZqwt";
    //     $check=DB::select(" SELECT * FROM `site_configuration` WHERE `token`='".$token."' ");
    //     //dd($check);
    //     foreach($check as $cc)
    //     {
    //            $trader_id=$cc->trader_id;
    //            $theme=$cc->theme;
    //            $_SESSION["trader_id"]=$trader_id;
    //            $_SESSION["theme"]=$theme;
    //     }
    //     $slider=CmsSlider::where("trader_id",$_SESSION["trader_id"])->where("type","mainslider")->get();
    //     $underslider=CmsSlider::where("trader_id",$_SESSION["trader_id"])->where("type","undermainslider")->get();
    //     $foodbanner=CmsSlider::where("trader_id",$_SESSION["trader_id"])->where("type","foodbanner")->get();
    //     $mobilebanner=CmsSlider::where("trader_id",$_SESSION["trader_id"])->where("type","mobilebanner")->get();
    //     $electronicbanner=CmsSlider::where("trader_id",$_SESSION["trader_id"])->where("type","electronicbanner")->get();
    //     $submainbanner=CmsSlider::where("trader_id",$_SESSION["trader_id"])->where("type","submainbanner")->get();
    //     //dd($_SESSION["trader_id"]);
    //     $product=Product::where("trader_id",$trader_id)->where("image","!=","zummXD2dvAtI.png")->limit(5)->get();
    //     $food=Product::where("trader_id",$trader_id)->where("image","!=","zummXD2dvAtI.png")->limit(4)->get();
    //     $mobile=Product::where("trader_id",$trader_id)->where("image","!=","zummXD2dvAtI.png")->limit(4)->get();
        
        
    //     if($_SESSION["theme"]==1)
    //     {
    //     return view("home",compact("product","food","mobile","slider","underslider","foodbanner","mobilebanner","electronicbanner","submainbanner"));
    //     }
    //     else if($_SESSION["theme"]==2)
    //     {
    //         return view("home2",compact("product","food","mobile","slider","underslider","foodbanner","mobilebanner"));
    //     }
    //     else
    //     {
    //         return view("home3",compact("product","food","mobile","slider","underslider","foodbanner","mobilebanner"));
    //     }
    // }
    public function searchbyitemname(Request $request)
    {
        //$itemname=$request->key;
        session_start();
        $option="";
        $option.='<div class="search-drop d-none d-xl-block">
        <table class="table table-image">
        <thead style="position: sticky;top: 0" class="thead-dark">
        <tr>
        <th scope="col">Image</th>
        <th scope="col">Item name</th>
        <th scope="col">Brand</th>
        <th scope="col">Price</th>
        </tr>
        </thead>
        <tbody>
        ';
           if(!empty($request->key))
           {
              $item=DB::select(" SELECT products.id,products.image,products.name,products.price,brands.title FROM `products` JOIN brands ON brands.id=products.brand_id WHERE products.`name` LIKE '%".$request->key."%' AND products.trader_id='".$_SESSION["trader_id"]."' LIMIT 100 ");
           if(!empty($item))
           {
   
           foreach($item as $it)
         {
            $img=asset("../../inventory/public/images/product/".$it->image);
           $nam=$it->name;
           $ro=route("productdetails",$it->id);
           $option.= "
  
           <tr class='tableitem'>
           <td class='table-image'><img style='width:120px;height:120px;' src=$img></td>
           <td> <a href=$ro>$it->name  </a></td>
           <td>$it->title</td>
           <td>$it->price</td>
       
           </tr>
 
           ";
       }
       $option.='</tbody>
       </table>
       </div>';
   
        }
   
       }
   
       return $option;
        // session_start();
        // $co=Cms::where("trader_id",$_SESSION["trader_id"])->get();
        // foreach($co as $c)
        // {
        //     $contact=$c->contact;
        // }
        //return view("pages.contact",compact("contact"));
    }

    public function addtocart(Request $request)
    {
        $itemcode=$request->itemcode;
        $product=Product::where("id",$itemcode)->get();
        foreach($product as $p)
        {
            $price=$p->price;
            $itemname=$p->name;
            $image=$p->image;
        }
        //$itemname=$request->itemname;
        //$price=$request->price;
        $quantity=$request->qty;
        $cart = session()->get('cart', []);
  
        if(isset($cart[$itemcode])) {
            $cart[$itemcode]['quantity']+=$quantity;
        } else {
            $cart[$itemcode] = [
                "name" => $itemname,
                "quantity" => $quantity,
                "price" => $price,
                "itemcode" => $itemcode,
                "image"=>$image
            ];
        }
          
        session()->put('cart', $cart);
        return $cart;
    }

    // public function remove(Request $request)
    // {
    //     $cart = session()->get('cart');
    //     if(isset($cart[$request->id])) {
    //         unset($cart[$request->id]);
    //         session()->put('cart', $cart);
    //     }
    // }

    public function modelcontent(Request $request)
    {
        $option="";
        if(session('cart'))
        {
        foreach(session('cart') as $id => $details)
        {
            $img=asset("../../inventory/public/images/product/".$details["image"]);
            $name=$details["name"];
            $quantity=$details["quantity"];
            $price=$details["price"];
        $option.="
        <div class='product-cart-son clearfix'>
        <div class='image-product-cart float-left center-vertical-image '>
            <a href='#'><img src=$img alt='' /></a>
        </div>
        <div class='info-product-cart float-left'>
            <p class='title-product title-hover-black'><a class='animate-default' href='#'>$name</a></p>
            <p class='clearfix price-product'>$$price<span class='total-product-cart-son'>(x$quantity)</span></p>
        </div>
    </div>
    ";
        }
    }
        return $option;
    }

    public function total(Request $request)
    {
        $total=0;
       // return $total;
        if(session('cart'))
        {
        foreach(session('cart') as $id => $details)
        {
        $total += $details['price'] * $details['quantity'];
        }
    }
        return $total;
    }

    public function itemcount()
    {
        $count=0;
        if(session('cart'))
        {
        foreach(session('cart') as $id => $details)
        {
        $count++;
        }
    }
        return $count;
    }
    public function remove($id)
    {
        $cart = session()->get('cart');
        //dd($cart[$id]);
        unset($cart[$id]);
        session()->put('cart', $cart);
        return redirect()->back();
    }

}
