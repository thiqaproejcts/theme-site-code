<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Product;
use App\SiteConfiguration;
use App\Cms;
use App\CmsSlider;
use App\Customer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
class RegisterController extends Controller
{
    public function register(Request $request)
    {
        //session_start();
        // $this->validate($request, [
           
        //     'email' => [
        //         'email',
        //         'max:255',
        //             Rule::unique('customers')->where(function ($query) {
        //             return $query->where('is_deleted', false);
        //         }),
        //     ],
        //     'phone_number' => [
        //         'max:255',
        //             Rule::unique('customers')->where(function ($query) {
        //             return $query->where('is_active', 1);
        //         }),
        //     ],
        // ]);

        $check=DB::table('customers')
                    ->where('email', $request->email)
                    ->get();
                    $check1=DB::table('customers')
                    ->Where('phone_number', $request->phone_number)
                    ->get();
if($check->count()>0)
{
    return redirect()->action("HomeController@login")->with("message","Email already taken");
}

if($check1->count()>0)
{
    return redirect()->action("HomeController@login")->with("message","Phone number already taken ");
}

        $name=$request->name;
        $username=$request->username;
        $postcode=$request->postcode;
        $address=$request->address;
        $email=$request->email;
        //$ordernote=$request->ordernote;
      //  $payment=$request->payment;
        $country=$request->country;
        $city=$request->city;
        $companyname=$request->companyname;
        $phone=$request->phone;

        $password=$request->password;
        $repassword=$request->retype;
        //$hash=Hash::make($password);
        $hash=hash('gost',$password); 
        if($password!=$repassword)
        {
            return redirect()->action("HomeController@login")->with("message","Your passwords do not match");
        }

       
        DB::select(" INSERT INTO `customers`( `customer_group_id`, `name`,`company_name`, `email`, `phone_number`,  `address`, `city`, `state`, `postal_code`, `country`,  `is_active`,  `password`) 
        VALUES 
        ('1','".$name."','".$companyname."','".$email."','".$phone."','".$address."','".$city."','','".$postcode."','".$country."','1','".$hash."') ");
        //Customer::create($lims_customer_data);
        return redirect()->action("HomeController@login")->with("success","Thankyou for registering.Please login now.");
        //$lims_customer_data['password'] = bcrypt($lims_customer_data['password']);
        // $hash=Hash::make($request->password);
        // $ch=Customer::where("name",$request->username)->where("password",$hash)->get();
        // if($ch->count()>0)
        // {
        //     foreach($ch as $cc)
        //     {
        //         $user_id=$cc->id;
        //     }
        //     $_SESSION["user"]=$user_id;
        //     return redirect()->action("HomeController@index");
        // }
        // else
        // {
        //     return redirect()->action("HomeController@login")->with("message","Your username or password doesnot match.");
        // }
    }
  
}