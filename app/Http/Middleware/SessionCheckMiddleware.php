<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
class SessionCheckMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        session_start();
        if(empty($_SESSION["userid"]))
        {
            
            return redirect()->action("HomeController@login");
        }
        else
        {
            //return redirect()->action("HomeController@checkout");
            //return redirect()->to('/checkout');
            return response()->view("pages.checkout");
        }
        //return $next($request);
    }
}
