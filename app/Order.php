<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Spatie\Activitylog\Traits\LogsActivity;

class Order extends Model
{
    //use LogsActivity;
    protected $fillable =[
        "reference_no", "user_id", "cash_register_id", "customer_id", "warehouse_id", "biller_id", "item", "total_qty", "total_discount", "total_tax", "total_price", "order_tax_rate", "order_tax", "order_discount","coupon_id","dalilee_order_id", "coupon_discount", "shipping_cost", "grand_total", "sale_status","order_status","payment_status", "paid_amount", "document", "sale_note", "staff_note","wilaya_id","region_id","is_delivery","collection_id","service_charges","trader_id","client_order","extra_charges","discount_type","order_weight","is_closed","closed_date","store_id","store_name"
    ];
    // protected static $logAttributes=[
    //     "reference_no", "user_id", "cash_register_id", "customer_id", "warehouse_id", "biller_id", "item", "total_qty", "total_discount", "total_tax", "total_price", "order_tax_rate", "order_tax", "order_discount","coupon_id","dalilee_order_id", "coupon_discount", "shipping_cost", "grand_total", "sale_status","order_status","payment_status", "paid_amount", "document", "sale_note", "staff_note","wilaya_id","region_id","is_delivery","collection_id","service_charges","trader_id","client_order","extra_charges","discount_type","order_weight","is_closed","closed_date","store_id","store_name"
    // ];

    // public function biller()
    // {
    // 	return $this->belongsTo('App\Biller');
    // }

    // public function customer()
    // {
    // 	return $this->belongsTo('App\Customer');
    // }

    // public function warehouse()
    // {
    // 	return $this->belongsTo('App\Warehouse');
    // }

    // public function user()
    // {
    // 	return $this->belongsTo('App\User');
    // }
}
