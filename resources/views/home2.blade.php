@extends('layout/app')
@section('content')
<?php
//dd($_SESSION["trader_id"]);
?>

	    <!-- Content Box -->
	    <div class="relative clearfix full-width">
	        <!-- Menu & Slide -->
	        <div class="clearfix container-web relative">
	            <div class=" container relative">
	                <div class="row">
	                    <div class=" relative menu-slide col-lg-12 clear-padding">
	                        <!-- Menu -->
	                        <div class=" menu-web relative">
	                            <ul>
	                                <li><a href="#"><img src="img/icon_hot.png" alt="Icon Hot Deals" /> <p>Hot Deals</p></a></li>
	                                <li><a href="#"><img src="img/icon_food.png" alt="Icon Food" /> <p>Food</p></a></li>
	                                <li><a href="#"><img src="img/icon_mobile.png" alt="Icon Mobile & Tablet" /> <p>Mobile & Tablet</p></a></li>
	                                <li><a href="#"><img src="img/icon_electric.png" alt="Icon Electric Appliances" /> <p>Electric Appliances</p></a></li>
	                                <li><a href="#"><img src="img/icon_computer.png" alt="Icon Electronics & Technology" /> <p>Electronics & Technology</p></a></li>
	                                <li><a href="#"><img src="img/icon_fashion.png" alt="Icon Fashion" /> <p>Fashion</p></a></li>
	                                <li><a href="#"><img src="img/icon_health.png" alt="Icon Health & Beauty" /> <p>Health & Beauty</p></a></li>
	                                <li><a href="#"><img src="img/icon_mother.png" alt="Icon Mother & Baby" /> <p>Mother & Baby</p></a></li>
	                                <li><a href="#"><img src="img/icon_book.png" alt="Icon Books & Stationery" /> <p>Books & Stationery</p></a></li>
	                                <li><a href="#"><img src="img/icon_tablet.png" alt="Icon Home & Life" /> <p>Home & Life</p></a></li>
	                                <li><a href="#"><img src="img/icon_sport.png" alt="Icon Sports & Outdoors" /> <p>Sports & Outdoors</p></a></li>
	                                <li><a href="#"><img src="img/icon_auto.png" alt="Icon Auto & Moto" /> <p>Auto & Moto</p></a></li>
	                                <li><a href="#"><img src="img/icon_voucher.png" alt="Icon Voucher Service" /> <p>Voucher Service</p></a></li>
	                            </ul>
	                        </div>
	                        <!-- Slide -->
	                        <div class=" slide-box-home slide-v2 relative">
	                            <div class=" slide-home owl-carousel owl-theme">
	                                <div class="item"><img src="img/banner_v2.png" alt="Banner Home V2"></div>
	                                <div class="item"><img src="img/banner_v2_2-min.png" alt="Banner Home V2"></div>
	                            </div>
	                        </div>
	                        <div class=" box-banner-small-v2 box-banner-small">
	                            <div class="relative effect-layla zoom-image-hover">
	                                <img src="img/banner_small_home_v2__1-min.png" alt="">
	                                <a href="#"></a>
	                            </div>
	                            <div class="relative effect-layla zoom-image-hover">
	                                <img src="img/banner_small_v2_1-min.png" alt="">
	                                <a href="#"></a>
	                            </div>
	                        </div>
	                    </div>
	                    <!-- End Menu & Slide -->
	                </div>
	            </div>
	        </div>
	        <!-- Content Product -->
	        <div class="clearfix box-product full-width top-padding-default bg-gray">
	            <div class="clearfix container-web">
	                <div class=" container bottom-margin-default">
	                    <div class="row">
	                        <!-- Title Product -->
	                        <div class="clearfix title-box full-width">
	                            <div class="clearfix name-title-box good-deals-v2 relative">
	                                <img src="img/icon_percent_color.png" alt="Good Deal Today" class="absolute clear-left" />
	                                <p class="text-default-color">good deal today</p>
	                            </div>
	                        </div>
	                        <div class="clearfix content-product-box bottom-margin-default full-width deal-hot-v2">
	                            <!-- Deals Content -->
	                            <div class="clearfix ">
	                                <div class="deals-col-2 banners-effect5 relative overfollow-hidden float-left">
	                                    <img src="img/deals_2_col.png" alt="Deals" />
	                                    <a href="#"></a>
	                                </div>
	                                <div class="deals-col-1 banners-effect5 relative overfollow-hidden float-left">
	                                    <img src="img/banner_deal_hot_-min.png" alt="Deals">
	                                    <a href="#"></a>
	                                </div>
	                                <div class="deals-col-1 banners-effect5 relative overfollow-hidden float-left">
	                                    <img src="img/deals_1_col.png" alt="Deals">
	                                    <a href="#"></a>
	                                </div>
	                            </div>
	                            <div class="clearfix ">
	                                <div class="deals-row-2 banners-effect5 clear-margin relative overfollow-hidden float-left">
	                                    <img src="img/deals_2_row.png" alt="Deals">
	                                    <a href="#"></a>
	                                </div>
	                                <div class="deals-col-1 banners-effect5 relative overfollow-hidden float-left">
	                                    <img src="img/banner_deal_hot_2-min.png" alt="Deals">
	                                    <a href="#"></a>
	                                </div>
	                                <div class="deals-col-1 banners-effect5 relative overfollow-hidden float-left">
	                                    <img src="img/banner_deal_hot_1-min.png" alt="Deals">
	                                    <a href="#"></a>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <!-- End Content Product -->
	        <!-- Product Box -->
			<div class="top-margin-default container-web">
                <div class=" container">
                    <div class="row">
                        <div class="clearfix title-box full-width border">
                            <div class="clearfix name-title-box title-category title-green-bg relative">
                                <img alt="Icon Food" src="{{asset('img/icon_food.png')}}" class="absolute" />
                                <p>food</p>
                            </div>
                            <div class="clearfix menu-title-box bold uppercase">
                                <ul>
                                    <li><a href="javascript:;" onclick="showBoxCateHomeByID('#confectionery','.box-food-content')">Confectionery</a></li>
                                    <li><a href="javascript:;" onclick="showBoxCateHomeByID('#milk-cream','.box-food-content')">Milk & Cream</a></li>
                                    <li><a href="javascript:;" onclick="showBoxCateHomeByID('#dry-food','.box-food-content')">Dry Food</a></li>
                                    <li><a href="javascript:;" onclick="showBoxCateHomeByID('#vegetables','.box-food-content')">Vegetables</a></li>
                                    <li><a href="javascript:;" onclick="showBoxCateHomeByID('#drinks','.box-food-content')">Drinks</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="display-table bottom-margin-default full-width">
                            <div class="clearfix clear-padding list-logo-category border no-border-t no-border-r list-logo-category-v1 float-left">
                                <ul>
                                    <li><a href="#"><img src="{{asset('img/logo_3.png')}}" alt="Logo"></a></li>
                                    <li><a href="#"><img src="{{asset('img/logo_4.png')}}" alt="Logo"></a></li>
                                    <li><a href="#"><img src="{{asset('img/logo_5.png')}}" alt="Logo"></a></li>
                                    <li><a href="#"><img src="{{asset('img/logo_6.png')}}" alt="Logo"></a></li>
                                    <li><a href="#"><img src="{{asset('img/logo_1.png')}}" alt="Logo"></a></li>
                                    <li><a href="#"><img src="{{asset('img/logo_2.png')}}" alt="Logo"></a></li>
                                </ul>
                            </div>
                            <div class=" banner-category banner-category-v1 float-left relative effect-bubba zoom-image-hover">
                                <?php
                                if(count($foodbanner)>0)
                                {
                                foreach($foodbanner as $fv)
                                    {
                                        $img="../../inventory/public/images/slider/".$fv->slider_img;
                                        //dd($img);
                                    }
                                }
                                else 
                                {
                                    $img="img/banner1.png";
                                }
                                ?>
                                <img src="{{asset($img)}}" alt="Banner">

                                <a href="#"></a>
                            </div>
                            <div class="clearfix list-products-category list-products-category-v1 float-left relative">
                                <div class="box-food-content relative animate-default active-box-category hidden-content-box border-collapsed-box" id="confectionery">
                                   <?php
                                    foreach($food as $f)
                                    {
                                        $img="../../inventory/public/images/product/".$f->image;
                                    ?>
                                    <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-3">
                                        <div class="effect-hover-zoom center-vertical-image">
                                            <img style="width:270px;height:270px;" src="{{asset($img)}}" alt="Product Image . . .">
                                            <a href="#"></a>
                                        </div>
                                        <div class="clearfix absolute name-product-no-ranking">
                                            <p class="title-product clearfix full-width title-hover-black"><a href="{{route('productdetails',$f->id)}}">{{$f->name}}</a></p>
                                            <p class="clearfix price-product"><span class="price-old">${{$f->cost}}</span> ${{$f->price}}</p>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                    
                                </div>
                                <div class="box-food-content relative animate-default hidden-content-box border-collapsed-box" id="milk-cream">
                                    <?php
                                    foreach($food as $f)
                                    {
                                        $img="../../inventory/public/images/product/".$f->image;
                                    ?>
                                    <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-3">
                                        <div class="effect-hover-zoom center-vertical-image">
                                            <img style="width:270px;height:270px;" src="{{asset($img)}}" alt="Product Image . . .">
                                            <a href="#"></a>
                                        </div>
                                        <div class="clearfix absolute name-product-no-ranking">
                                            <p class="title-product clearfix full-width title-hover-black"><a href="{{route('productdetails',$f->id)}}">{{$f->name}}</a></p>
                                            <p class="clearfix price-product"><span class="price-old">${{$f->cost}}</span> ${{$f->price}}</p>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div class="box-food-content relative animate-default hidden-content-box border-collapsed-box" id="dry-food">
                                    <?php
                                    foreach($food as $f)
                                    {
                                        $img="../../inventory/public/images/product/".$f->image;
                                    ?>
                                    <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-3">
                                        <div class="effect-hover-zoom center-vertical-image">
                                            <img style="width:270px;height:270px;" src="{{asset($img)}}" alt="Product Image . . .">
                                            <a href="#"></a>
                                        </div>
                                        <div class="clearfix absolute name-product-no-ranking">
                                            <p class="title-product clearfix full-width title-hover-black"><a href="{{route('productdetails',$f->id)}}">{{$f->name}}</a></p>
                                            <p class="clearfix price-product"><span class="price-old">${{$f->cost}}</span> ${{$f->price}}</p>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div class="box-food-content relative animate-default hidden-content-box border-collapsed-box" id="vegetables">
                                    <?php
                                    foreach($food as $f)
                                    {
                                        $img="../../inventory/public/images/product/".$f->image;
                                    ?>
                                    <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-3">
                                        <div class="effect-hover-zoom center-vertical-image">
                                            <img style="width:270px;height:270px;" src="{{asset($img)}}" alt="Product Image . . .">
                                            <a href="#"></a>
                                        </div>
                                        <div class="clearfix absolute name-product-no-ranking">
                                            <p class="title-product clearfix full-width title-hover-black"><a href="{{route('productdetails',$f->id)}}">{{$f->name}}</a></p>
                                            <p class="clearfix price-product"><span class="price-old">${{$f->cost}}</span> ${{$f->price}}</p>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div class="box-food-content relative animate-default hidden-content-box border-collapsed-box" id="drinks">
                                    <?php
                                    foreach($food as $f)
                                    {
                                        $img="../../inventory/public/images/product/".$f->image;
                                    ?>
                                    <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-3">
                                        <div class="effect-hover-zoom center-vertical-image">
                                            <img style="width:270px;height:270px;" src="{{asset($img)}}" alt="Product Image . . .">
                                            <a href="#"></a>
                                        </div>
                                        <div class="clearfix absolute name-product-no-ranking">
                                            <p class="title-product clearfix full-width title-hover-black"><a href="{{route('productdetails',$f->id)}}">{{$f->name}}</a></p>
                                            <p class="clearfix price-product"><span class="price-old">${{$f->cost}}</span> ${{$f->price}}</p>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Product Box -->
	        <!-- End Product Box -->
	        <!-- Banner Full With -->
	        <div class="clearfix relative full-width bottom-margin-default">
	            <div class="clearfix container-web">
	                <div class=" container banner_full_width">
	                    <div class="row overfollow-hidden banners-effect5 relative">
	                        <img src="img/banner_full_w.png" alt="Banner Full Width . . .">
	                        <a href="#"></a>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <!-- End Banner Full With -->
	        <!-- Product Box -->
			<div class=" container-web">
                <div class=" container">
                    <div class="row">
                        <div class="clearfix title-box full-width border">
                            <div class="clearfix name-title-box title-category title-jungle-green-bg relative">
                                <img alt="Icon Mobile & Tablet" src="{{asset('img/icon_mobile.png')}}" class="absolute" />
                                <p>mobile & tablet</p>
                            </div>
                            <div class="clearfix menu-title-box bold uppercase">
                                <ul>
                                    <li><a onclick="showBoxCateHomeByID('#smart-phone','.box-mobile-content')" href="javascript:;">Smart phone</a></li>
                                    <li><a onclick="showBoxCateHomeByID('#tablet','.box-mobile-content')" href="javascript:;">Tablet</a></li>
                                    <li><a onclick="showBoxCateHomeByID('#smart-watch','.box-mobile-content')" href="javascript:;">Smart Watch</a></li>
                                    <li><a onclick="showBoxCateHomeByID('#case','.box-mobile-content')" href="javascript:;">Case</a></li>
                                    <li><a onclick="showBoxCateHomeByID('#gadget','.box-mobile-content')" href="javascript:;">Gadget</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="display-table bottom-margin-default full-width">
                            <div class="clearfix clear-padding list-logo-category list-logo-category-v1 float-left border no-border-t no-border-r">
                                <ul>
                                    <li><a href="#"><img src="{{asset('img/logo_3.png')}}" alt="Logo"></a></li>
                                    <li><a href="#"><img src="{{asset('img/logo_4.png')}}" alt="Logo"></a></li>
                                    <li><a href="#"><img src="{{asset('img/logo_5.png')}}" alt="Logo"></a></li>
                                    <li><a href="#"><img src="{{asset('img/logo_6.png')}}" alt="Logo"></a></li>
                                    <li><a href="#"><img src="{{asset('img/logo_1.png')}}" alt="Logo"></a></li>
                                    <li><a href="#"><img src="{{asset('img/logo_2.png')}}" alt="Logo"></a></li>
                                </ul>
                            </div>
                            <div class=" banner-category float-left relative effect-bubba zoom-image-hover">
                                <?php
                                if(count($mobilebanner)>0)
                                {
                                foreach($mobilebanner as $mv)
                                    {
                                        $img="../../inventory/public/images/slider/".$mv->slider_img;
                                        //dd($img);
                                    }
                                }
                                else 
                                {
                                    $img="img/banner1.png";
                                }
                                ?>
                                <img src="{{asset($img)}}" alt="Banner">
                                <a href="#"></a>
                            </div>
                            <div class="clearfix list-products-category list-products-category-v1 float-left relative">
                                <div class="box-mobile-content border-collapsed-box animate-default hidden-content-box active-box-category" id="smart-phone">
                                   <?php
                                    foreach($mobile as $m)
                                    {
                                        $img="../../inventory/public/images/product/".$m->image;
                                    ?>
                                    <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-3">
                                        <div class="effect-hover-zoom center-vertical-image">
                                            <img style="width:270px;height:270px;" src="{{asset($img)}}" alt="Product Image . . .">
                                            <a href="#"></a>
                                        </div>
                                        <div class="clearfix absolute name-product-no-ranking">
                                            <p class="title-product clearfix full-width title-hover-black"><a href="{{route('productdetails',$m->id)}}">{{$m->name}}</a></p>
                                            <p class="clearfix price-product"><span class="price-old">${{$m->cost}}</span> ${{$m->price}}</p>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div class="box-mobile-content border-collapsed-box animate-default hidden-content-box" id="tablet">
                                    <?php
                                    foreach($mobile as $m)
                                    {
                                        $img="../../inventory/public/images/product/".$m->image;
                                    ?>
                                    <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-3">
                                        <div class="effect-hover-zoom center-vertical-image">
                                            <img style="width:270px;height:270px;" src="{{asset($img)}}" alt="Product Image . . .">
                                            <a href="#"></a>
                                        </div>
                                        <div class="clearfix absolute name-product-no-ranking">
                                            <p class="title-product clearfix full-width title-hover-black"><a href="{{route('productdetails',$m->id)}}">{{$m->name}}</a></p>
                                            <p class="clearfix price-product"><span class="price-old">${{$m->cost}}</span> ${{$m->price}}</p>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div class="box-mobile-content border-collapsed-box animate-default hidden-content-box" id="smart-watch">
                                    <?php
                                    foreach($mobile as $m)
                                    {
                                        $img="../../inventory/public/images/product/".$m->image;
                                    ?>
                                    <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-3">
                                        <div class="effect-hover-zoom center-vertical-image">
                                            <img style="width:270px;height:270px;" src="{{asset($img)}}" alt="Product Image . . .">
                                            <a href="#"></a>
                                        </div>
                                        <div class="clearfix absolute name-product-no-ranking">
                                            <p class="title-product clearfix full-width title-hover-black"><a href="{{route('productdetails',$m->id)}}">{{$m->name}}</a></p>
                                            <p class="clearfix price-product"><span class="price-old">${{$m->cost}}</span> ${{$m->price}}</p>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div class="box-mobile-content border-collapsed-box animate-default hidden-content-box" id="case">
                                    <?php
                                    foreach($mobile as $m)
                                    {
                                        $img="../../inventory/public/images/product/".$m->image;
                                    ?>
                                    <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-3">
                                        <div class="effect-hover-zoom center-vertical-image">
                                            <img style="width:270px;height:270px;" src="{{asset($img)}}" alt="Product Image . . .">
                                            <a href="#"></a>
                                        </div>
                                        <div class="clearfix absolute name-product-no-ranking">
                                            <p class="title-product clearfix full-width title-hover-black"><a href="{{route('productdetails',$m->id)}}">{{$m->name}}</a></p>
                                            <p class="clearfix price-product"><span class="price-old">${{$m->cost}}</span> ${{$m->price}}</p>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div class="box-mobile-content border-collapsed-box animate-default hidden-content-box" id="gadget">
                                    <?php
                                    foreach($mobile as $m)
                                    {
                                        $img="../../inventory/public/images/product/".$m->image;
                                    ?>
                                    <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-3">
                                        <div class="effect-hover-zoom center-vertical-image">
                                            <img style="width:270px;height:270px;" src="{{asset($img)}}" alt="Product Image . . .">
                                            <a href="#"></a>
                                        </div>
                                        <div class="clearfix absolute name-product-no-ranking">
                                            <p class="title-product clearfix full-width title-hover-black"><a href="{{route('productdetails',$m->id)}}">{{$m->name}}</a></p>
                                            <p class="clearfix price-product"><span class="price-old">${{$m->cost}}</span> ${{$m->price}}</p>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	        <!-- End Product Box -->
	        <!-- Banner Full With -->
	        <div class="clearfix relative full-width bottom-margin-default">
	            <div class="clearfix container-web">
	                <div class=" container banner_full_width">
	                    <div class="row relative banners-effect5 overfollow-hidden">
	                        <img src="img/banner_full_w_1.png" alt="Banner Full Width . . .">
	                        <a href="#"></a>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <!-- End Banner Full With -->
	        <!-- Product Box -->
			<div class=" container-web">
                <div class=" container">
                    <div class="row">
                        <div class="clearfix title-box full-width border">
                            <div class="clearfix name-title-box title-category title-turquoise-bg relative">
                                <img alt="Icon Electric" src="{{asset('img/icon_electric.png')}}" class="absolute" />
                                <p>Electric</p>
                            </div>
                            <div class="clearfix menu-title-box bold uppercase">
                                <ul>
                                    <li><a onclick="showBoxCateHomeByID('#television','.box-electric-content')" href="javascript:;">television</a></li>
                                    <li><a onclick="showBoxCateHomeByID('#laptop','.box-electric-content')" href="javascript:;">laptop</a></li>
                                    <li><a onclick="showBoxCateHomeByID('#camera','.box-electric-content')" href="javascript:;">camera</a></li>
                                    <li><a onclick="showBoxCateHomeByID('#audio','.box-electric-content')" href="javascript:;">audio</a></li>
                                    <li><a onclick="showBoxCateHomeByID('#accessories','.box-electric-content')" href="javascript:;">Accessories</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="display-table bottom-margin-default full-width">
                            <div class="clearfix clear-padding list-logo-category border no-border-t no-border-r list-logo-category-v1 float-left">
                                <ul>
                                    <li><a href="#"><img src="{{asset('img/logo_3.png')}}" alt="Logo"></a></li>
                                    <li><a href="#"><img src="{{asset('img/logo_4.png')}}" alt="Logo"></a></li>
                                    <li><a href="#"><img src="{{asset('img/logo_5.png')}}" alt="Logo"></a></li>
                                    <li><a href="#"><img src="{{asset('img/logo_6.png')}}" alt="Logo"></a></li>
                                    <li><a href="#"><img src="{{asset('img/logo_1.png')}}" alt="Logo"></a></li>
                                    <li><a href="#"><img src="{{asset('img/logo_2.png')}}" alt="Logo"></a></li>
                                </ul>
                            </div>
                            <div class=" banner-category float-left relative zoom-image-hover effect-bubba">
                                <?php
if(count($electronicbanner)>0)
{
                                foreach($electronicbanner as $ev)
                                    {
                                        $img="../../inventory/public/images/slider/".$ev->slider_img;
                                        //dd($img);
                                    }
                                }
                                    else 
                                {
                                    $img="img/banner1.png";
                                }
                                ?>
                                <img src="{{asset($img)}}" alt="Banner">
                                <a href="#"></a>
                            </div>
                            <div class="clearfix list-products-category list-products-category-v1 float-left relative">
                                <div class="border-collapsed-box active-box-category hidden-content-box box-electric-content animate-default" id="television">
                                   <?php
                                    foreach($mobile as $m)
                                    {
                                        $img="../../inventory/public/images/product/".$m->image;
                                    ?>
                                    <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-3">
                                        <div class="effect-hover-zoom center-vertical-image">
                                            <img style="width:270px;height:270px;" src="{{asset($img)}}" alt="Product Image . . .">
                                            <a href="#"></a>
                                        </div>
                                        <div class="clearfix absolute name-product-no-ranking">
                                            <p class="title-product clearfix full-width title-hover-black"><a href="{{route('productdetails',$m->id)}}">{{$m->name}}</a></p>
                                            <p class="clearfix price-product"><span class="price-old">${{$m->cost}}</span> ${{$m->price}}</p>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                    
                                </div>
                                <div class="border-collapsed-box hidden-content-box box-electric-content animate-default" id="laptop">
                                    <?php
                                    foreach($mobile as $m)
                                    {
                                        $img="../../inventory/public/images/product/".$m->image;
                                    ?>
                                    <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-3">
                                        <div class="effect-hover-zoom center-vertical-image">
                                            <img style="width:270px;height:270px;" src="{{asset($img)}}" alt="Product Image . . .">
                                            <a href="#"></a>
                                        </div>
                                        <div class="clearfix absolute name-product-no-ranking">
                                            <p class="title-product clearfix full-width title-hover-black"><a href="{{route('productdetails',$m->id)}}">{{$m->name}}</a></p>
                                            <p class="clearfix price-product"><span class="price-old">${{$m->cost}}</span> ${{$m->price}}</p>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div class="border-collapsed-box hidden-content-box box-electric-content animate-default" id="camera">
                                    <?php
                                    foreach($mobile as $m)
                                    {
                                        $img="../../inventory/public/images/product/".$m->image;
                                    ?>
                                    <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-3">
                                        <div class="effect-hover-zoom center-vertical-image">
                                            <img style="width:270px;height:270px;" src="{{asset($img)}}" alt="Product Image . . .">
                                            <a href="#"></a>
                                        </div>
                                        <div class="clearfix absolute name-product-no-ranking">
                                            <p class="title-product clearfix full-width title-hover-black"><a href="{{route('productdetails',$m->id)}}">{{$m->name}}</a></p>
                                            <p class="clearfix price-product"><span class="price-old">${{$m->cost}}</span> ${{$m->price}}</p>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div class="border-collapsed-box hidden-content-box box-electric-content animate-default" id="audio">
                                    <?php
                                    foreach($mobile as $m)
                                    {
                                        $img="../../inventory/public/images/product/".$m->image;
                                    ?>
                                    <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-3">
                                        <div class="effect-hover-zoom center-vertical-image">
                                            <img style="width:270px;height:270px;" src="{{asset($img)}}" alt="Product Image . . .">
                                            <a href="#"></a>
                                        </div>
                                        <div class="clearfix absolute name-product-no-ranking">
                                            <p class="title-product clearfix full-width title-hover-black"><a href="{{route('productdetails',$m->id)}}">{{$m->name}}</a></p>
                                            <p class="clearfix price-product"><span class="price-old">${{$m->cost}}</span> ${{$m->price}}</p>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div class="border-collapsed-box hidden-content-box box-electric-content animate-default" id="accessories">
                                    <?php
                                    foreach($mobile as $m)
                                    {
                                        $img="../../inventory/public/images/product/".$m->image;
                                    ?>
                                    <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-3">
                                        <div class="effect-hover-zoom center-vertical-image">
                                            <img style="width:270px;height:270px;" src="{{asset($img)}}" alt="Product Image . . .">
                                            <a href="#"></a>
                                        </div>
                                        <div class="clearfix absolute name-product-no-ranking">
                                            <p class="title-product clearfix full-width title-hover-black"><a href="{{route('productdetails',$m->id)}}">{{$m->name}}</a></p>
                                            <p class="clearfix price-product"><span class="price-old">${{$m->cost}}</span> ${{$m->price}}</p>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	        <!-- End Product Box -->
	        <!-- Banner Half Website -->
	        <div class=" relative banner-half-web full-width bottom-margin-default">
	            <div class="clearfix container-web">
	                <div class=" container">
	                    <div class="row">
	                        <div class="clearfix content-left col-md-6 col-sm-6 col-xs-12 zoom-image-hover overfollow-hidden">
	                            <div class="overfollow-hidden effect-oscar relative">
	                                <img class="max-width" src="img/banner_halt.png" alt="Banner . . ." />
	                                <a href="#"></a>
	                            </div>
	                        </div>
	                        <div class="clearfix content-right col-md-6 col-sm-6 col-xs-12 zoom-image-hover overfollow-hidden">
	                            <div class="overfollow-hidden effect-oscar relative">
	                                <img class="max-width" src="img/banner_percent_2.png" alt="Banner . . ." />
	                                <a href="#"></a>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <!-- Product Category Percent 2 -->
			<div class=" full-width category-percent-two bottom-margin-default">
                <div class="clearfix container-web">
                    <div class=" container">
                        <div class="row">
                            <div class=" clearfix content-left col-md-6 col-sm-6">
                                <!-- Title Product -->
                                <div class="clearfix title-box full-width border">
                                    <div class="clearfix name-title-box title-category title-gold-bg relative">
                                        <img src="{{asset('img/icon_fashion.png')}}" alt="Icon Fashion" class="absolute" />
                                        <p>fashion</p>
                                    </div>
                                    <div class="clearfix menu-title-box">
                                        <p class="view-all-product-category title-hover-red"><a href="#" class="animate-default">view all</a></p>
                                    </div>
                                </div>
                                <div class=" banner-percent-product zoom-image-hover overfollow-hidden effect-oscar relative">
                                    <img src="{{asset('img/banner_product_percent.png')}}" class="max-width" alt="Image . . ." />
                                    <a href="#"></a>
                                </div>
                                <!-- Content Product Box -->
                                <div class="clearfix product-percent-content border-collapsed-box full-width">
                                    <?php
                                    foreach($mobile as $m)
                                    {
                                        $img="../../inventory/public/images/product/".$m->image;
                                    ?>
                                    <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-3">
                                        <div class="effect-hover-zoom center-vertical-image">
                                            <img style="width:270px;height:270px;" src="{{asset($img)}}" alt="Product Image . . .">
                                            <a href="#"></a>
                                        </div>
                                        <div class="clearfix absolute name-product-no-ranking">
                                            <p class="title-product clearfix full-width title-hover-black"><a href="{{route('productdetails',$m->id)}}">{{$m->name}}</a></p>
                                            <p class="clearfix price-product"><span class="price-old">${{$m->cost}}</span> ${{$m->price}}</p>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                    
                                </div>
                            </div>
                            <div class=" clearfix content-right col-md-6 col-sm-6">
                                <!-- Title Product -->
                                <div class="clearfix title-box full-width border">
                                    <div class="clearfix name-title-box title-category title-violet-bg relative">
                                        <img src="{{asset('img/icon_health.png')}}" alt="Icon Health & Beauty" class="absolute" />
                                        <p>health & beauty</p>
                                    </div>
                                    <div class="clearfix menu-title-box">
                                        <p class="view-all-product-category title-hover-red"><a href="#" class="animate-default">view all</a></p>
                                    </div>
                                </div>
                                <div class=" banner-percent-product zoom-image-hover overfollow-hidden effect-oscar relative">
                                    <img src="{{asset('img/banner_p_2_1.png')}}" class="max-width" alt="Image . . ." />
                                    <a href="#"></a>
                                </div>
                                <!-- Content Product Box -->
                                <div class="clearfix product-percent-content border-collapsed-box full-width">
                                   <?php
                                    foreach($mobile as $m)
                                    {
                                        $img="../../inventory/public/images/product/".$m->image;
                                    ?>
                                    <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-3">
                                        <div class="effect-hover-zoom center-vertical-image">
                                            <img style="width:270px;height:270px;" src="{{asset($img)}}" alt="Product Image . . .">
                                            <a href="#"></a>
                                        </div>
                                        <div class="clearfix absolute name-product-no-ranking">
                                            <p class="title-product clearfix full-width title-hover-black"><a href="{{route('productdetails',$m->id)}}">{{$m->name}}</a></p>
                                            <p class="clearfix price-product"><span class="price-old">${{$m->cost}}</span> ${{$m->price}}</p>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	        <!-- End Product Category Percent 2 -->
	        <!-- Product Category Percent 2 -->
			<div class=" full-width category-percent-two bottom-margin-default">
                <div class="clearfix container-web">
                    <div class=" container">
                        <div class="row">
                            <div class=" clearfix content-left col-md-6 col-sm-6">
                                <!-- Title Product -->
                                <div class="clearfix title-box full-width border">
                                    <div class="clearfix name-title-box title-category title-magenta-bg relative">
                                        <img src="{{asset('img/icon_mother.png')}}" alt="Icon Mother" class="absolute">
                                        <p>mother & baby</p>
                                    </div>
                                    <div class="clearfix menu-title-box">
                                        <p class="view-all-product-category title-hover-red"><a href="#" class="animate-default">view all</a></p>
                                    </div>
                                </div>
                                <div class=" banner-percent-product overfollow-hidden zoom-image-hover effect-oscar relative">
                                    <img src="{{asset('img/banner_p_2_2.png')}}" class="max-width" alt="Image . . ." />
                                    <a href="#"></a>
                                </div>
                                <!-- Content Product Box -->
                                <div class="clearfix product-percent-content border-collapsed-box full-width">
                                    <?php
                                    foreach($mobile as $m)
                                    { 
                                        $img="../../inventory/public/images/product/".$m->image;
                                    ?>
                                    <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-3">
                                        <div class="effect-hover-zoom center-vertical-image">
                                            <img style="width:270px;height:270px;" src="{{asset($img)}}" alt="Product Image . . .">
                                            <a href="#"></a>
                                        </div>
                                        <div class="clearfix absolute name-product-no-ranking">
                                            <p class="title-product clearfix full-width title-hover-black"><a href="{{route('productdetails',$m->id)}}">{{$m->name}}</a></p>
                                            <p class="clearfix price-product"><span class="price-old">${{$m->cost}}</span> ${{$m->price}}</p>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                    
                                </div>
                            </div>
                            <div class=" clearfix content-right col-md-6 col-sm-6">
                                <!-- Title Product -->
                                <div class="clearfix title-box full-width border">
                                    <div class="clearfix name-title-box title-category title-orchild-bg relative">
                                        <img src="{{asset('img/icon_auto.png')}}" alt="Icon Auto" class="absolute">
                                        <p>auto & moto</p>
                                    </div>
                                    <div class="clearfix menu-title-box">
                                        <p class="view-all-product-category title-hover-red"><a href="#" class="animate-default">view all</a></p>
                                    </div>
                                </div>
                                <div class=" banner-percent-product overfollow-hidden zoom-image-hover effect-oscar relative">
                                    <img src="{{asset('img/banner_p_2_3.png')}}" class="max-width" alt="Image . . ." />
                                    <a href="#"></a>
                                </div>
                                <!-- Content Product Box -->
                                <div class="clearfix product-percent-content border-collapsed-box full-width">
                                    <?php
                                    foreach($mobile as $m)
                                    {
                                        $img="../../inventory/public/images/product/".$m->image;
                                    ?>
                                    <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-3">
                                        <div class="effect-hover-zoom center-vertical-image">
                                            <img style="width:270px;height:270px;" src="{{asset($img)}}" alt="Product Image . . .">
                                            <a href="#"></a>
                                        </div>
                                        <div class="clearfix absolute name-product-no-ranking">
                                            <p class="title-product clearfix full-width title-hover-black"><a href="{{route('productdetails',$m->id)}}">{{$m->name}}</a></p>
                                            <p class="clearfix price-product"><span class="price-old">${{$m->cost}}</span> ${{$m->price}}</p>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	        <!-- End Product Category Percent 2 -->
	        <!-- Slide Logo Brand -->
	        <div class=" slide-brand-box full-width bottom-margin-default">
	            <div class="clearfix container-web relative">
	                <div class=" container">
	                    <div class="row">
	                        <div class="nav-prev nav-slide-brand"></div>
	                        <div class="slide-logo-brand col-md-12 clear-padding relative owl-theme owl-carousel border-collapsed-box">
	                            <div class="item">
	                                <div class="clearfix border-collapsed-element relative logo-brand-son"><img src="img/logo_3.png" alt="Logo"></div>
	                                <div class="clearfix border-collapsed-element relative logo-brand-son"><img src="img/logo_7.png" alt="Logo"></div>
	                            </div>
	                            <div class="item">
	                                <div class="clearfix border-collapsed-element relative logo-brand-son"><img src="img/logo_4.png" alt="Logo"></div>
	                                <div class="clearfix border-collapsed-element relative logo-brand-son"><img src="img/logo_8.png" alt="Logo"></div>
	                            </div>
	                            <div class="item">
	                                <div class="clearfix border-collapsed-element relative logo-brand-son"><img src="img/logo_5.png" alt="Logo"></div>
	                                <div class="clearfix border-collapsed-element relative logo-brand-son"><img src="img/logo_9.png" alt="Logo"></div>
	                            </div>
	                            <div class="item">
	                                <div class="clearfix border-collapsed-element relative logo-brand-son"><img src="img/logo_6.png" alt="Logo" /></div>
	                                <div class="clearfix border-collapsed-element relative logo-brand-son"><img src="img/logo_10.png" alt="Logo" /></div>
	                            </div>
	                            <div class="item">
	                                <div class="clearfix border-collapsed-element relative logo-brand-son"><img src="img/logo_1.png" alt="Logo" /></div>
	                                <div class="clearfix border-collapsed-element relative logo-brand-son"><img src="img/logo_11.png" alt="Logo" /></div>
	                            </div>
	                            <div class="item">
	                                <div class="clearfix border-collapsed-element relative logo-brand-son"><img src="img/logo_2.png" alt="Logo" /></div>
	                                <div class="clearfix border-collapsed-element relative logo-brand-son"><img src="img/logo_12.png" alt="Logo" /></div>
	                            </div>
	                        </div>
	                        <div class="nav-next nav-slide-brand"></div>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <!-- End Slide Brand -->
	        <!-- Banner Full With -->
	        <div class=" relative full-width bottom-margin-default">
	            <div class="clearfix container-web">
	                <div class=" container banner_full_width">
	                    <div class="row overfollow-hidden banners-effect5 relative">
	                        <img src="img/banner_cate_f_w_3-min.png" alt="Banner Full Width . . .">
	                        <a href="#"></a>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <!-- End Banner Full With -->
	        <!-- Support -->
	        <div class=" support-box full-width bg-red support_box_v2">
	            <div class="container-web ">
	                <div class=" container">
	                    <div class="row">
	                        <div class=" support-box-info relative col-md-3 support-box-v2 col-sm-3 col-xs-6">
	                            <img src="img/icon_free_ship_white-min.png" alt="Icon Free Ship" class="absolute" />
	                            <p>free shipping</p>
	                            <p>on order over $500</p>
	                        </div>
	                        <div class=" support-box-info relative col-md-3 support-box-v2 col-sm-3 col-xs-6">
	                            <img src="img/icon_support_white-min.png" alt="Icon Supports" class="absolute">
	                            <p>support</p>
	                            <p>life time support 24/7</p>
	                        </div>
	                        <div class=" support-box-info relative col-md-3 support-box-v2 col-sm-3 col-xs-6">
	                            <img src="img/icon_patner_white-min.png" alt="Icon partner" class="absolute">
	                            <p>help partner</p>
	                            <p>help all aspects</p>
	                        </div>
	                        <div class=" support-box-info relative col-md-3 support-box-v2 col-sm-3 col-xs-6">
	                            <img src="img/icon_phone_table_white-min.png" alt="Icon Phone Tablet" class="absolute">
	                            <p>contact with us</p>
	                            <p>+07 (0) 7782 9137</p>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- End Content Box -->
@endsection