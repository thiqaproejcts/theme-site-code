@extends("layout.app")
@section("content")
    <!-- Content Box -->
	<div class="relative full-width">
		<!-- Breadcrumb -->
		<div class="container-web relative">
			<div class="container">
				<div class="row">
					<div class="breadcrumb-web">
						<ul class="clear-margin">
							<li class="animate-default title-hover-red"><a href="#">Home</a></li>
							<li class="animate-default title-hover-red"><a href="#">Shoping Cart</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- End Breadcrumb -->
		<!-- Content Shoping Cart -->
		<div class="relative container-web">
			<div class="container">
				<div class="row relative">
					<!-- Content Shoping Cart -->
					<div class="col-md-8 col-sm-12 col-xs-12 relative left-content-shoping clear-padding-left">
						<p class="title-shoping-cart">Shopping Cart</p>
						<?php
						if(session('cart'))
        {
						foreach(session('cart') as $id => $details)
						{
							$img="../../inventory/public/images/product/".$details["image"];
						?>
						<div class="relative full-width product-in-cart border no-border-l no-border-r overfollow-hidden">
							<div class="relative product-in-cart-col-1 center-vertical-image">
								<img src="{{asset($img)}}" alt="">
							</div>
							<div class="relative product-in-cart-col-2">
								<p class="title-hover-black"><a href="{{route('productdetails',$details["itemcode"])}}" class="animate-default">{{$details["name"]}}</a>
									<br>Quantity:&nbsp;&nbsp;&nbsp;{{$details["quantity"]}}
								<br>Price:&nbsp;&nbsp;&nbsp;{{$details["price"]}}
								</p>
								
							</div>
							<div class="relative product-in-cart-col-3">
								<a href="{{route('remove',$details["itemcode"])}}" onclick="return confirm('Are you sure?')"><span class="ti-close relative remove-product"></span></a>
								<p class="text-red price-shoping-cart">${{$details["price"]*$details["quantity"]}}</p>
							</div>
						</div>
						<?php
						}
					}
					else {
						?>
					<div class="relative full-width product-in-cart border no-border-l no-border-r overfollow-hidden">
						{{-- <div class="relative product-in-cart-col-1 center-vertical-image">
							<img src="{{asset($img)}}" alt="">
						</div> --}}
						<div class="relative product-in-cart-col-2" style="height:200px;">
							<p style="text-align: center;margin-top:100px;" class="title-product top-margin-15-default animate-default title-hover-black"><a href="#" class="animate-default">Your cart is empty</a>
								{{-- <br>Quantity:&nbsp;&nbsp;&nbsp;{{$details["quantity"]}}
							<br>Price:&nbsp;&nbsp;&nbsp;{{$details["price"]}} --}}
							</p>
							
						</div>
						{{-- <div class="relative product-in-cart-col-3">
							<a href="{{route('remove',$details["itemcode"])}}" onclick="return confirm('Are you sure?')"><span class="ti-close relative remove-product"></span></a>
							<p class="text-red price-shoping-cart">${{$details["price"]*$details["quantity"]}}</p>
						</div> --}}
					</div>
					<?php
					}
					?>
						<aside class="btn-shoping-cart justify-content top-margin-default bottom-margin-default">
							<a href="{{route('home')}}" class="clear-margin animate-default">Continue Shopping</a>
							<a href="#" class="clear-margin animate-default">Update Cart</a>
						</aside>
					</div>
					<!-- End Content Shoping Cart -->
					<!-- Content Right -->
					<div class="col-md-4 col-sm-12 col-xs-12 right-content-shoping relative clear-padding-right">
						<p class="title-shoping-cart">Coupon</p>
						<div class="full-width relative coupon-code bg-gray  clearfix">
							<input type="text" class="full-width" name="coupon_code" placeholder="Coupon Code">
							<button class="full-width top-margin-15-default">APPLY COUPON</button>
						</div>
						<p class="title-shoping-cart">Cart Total</p>
						<div class="full-width relative cart-total bg-gray  clearfix">
							<div class="relative justify-content bottom-padding-15-default border no-border-t no-border-r no-border-l">
								<p>Subtotal</p>
								<p class="text-red price-shoping-cart " id="totalamount"></p>
							</div>
							<div class="relative border top-margin-15-default bottom-padding-15-default no-border-t no-border-r no-border-l">
								<p class="bottom-margin-15-default">Shipping</p>
								<div class="relative justify-content">
									<ul class="check-box-custom title-check-box-black clear-margin clear-margin">
										<li>
											<label>Free Shipping
												<input type="radio" name="shipping" checked="">
			  									<span class="checkmark"></span>
			  								</label>
										</li>
										<li>
											<label>Standard
												<input type="radio" name="shipping">
			  									<span class="checkmark"></span>
			  								</label>
										</li>
										<li>
											<label>Local Pickup
												<input type="radio" name="shipping">
			  									<span class="checkmark"></span>
			  								</label>
										</li>
									</ul>
									<p class="price-gray-sidebar">$20.00</p>
								</div>
								<div onclick="optionShiping(this)" class="relative full-width select-ship-option justify-content top-margin-15-default">
									<p class="border no-border-r no-border-l no-border-t">Calculate Shipping</p>
									<i class="fa fa-caret-down" aria-hidden="true"></i>
									<ul class="clear-margin absolute full-width">
										<li onclick="selectOptionShoping(this)">Calculate Shipping 1</li>
										<li onclick="selectOptionShoping(this)">Calculate Shipping 2</li>
										<li onclick="selectOptionShoping(this)">Calculate Shipping 3</li>
									</ul>
								</div>
							</div>
							<div class="relative justify-content top-margin-15-default">
								<p class="bold">Total</p>
								<p class="text-red price-shoping-cart " id="totalamountall"></p>
							</div>
						</div>
						<form method="GET" action="{{route('checkout')}}">
						<button type="submit" class="btn-proceed-checkout button-hover-red full-width top-margin-15-default">Proceed to Checkout</button>
						</form>
					</div>
					<!-- End Content Right -->
				</div>
			</div>
		</div>
		<!-- End Content Shoping Cart -->
		<!-- Support -->
		<div class=" support-box full-width bg-red support_box_v2">
			<div class="container-web">
				<div class=" container">
					<div class="row">
						<div class=" support-box-info relative col-md-3 col-sm-3 col-xs-6">
							<img src="img/icon_free_ship_white-min.png" alt="Icon Free Ship" class="absolute" />
							<p>free shipping</p>
							<p>on order over $500</p>
						</div>
						<div class=" support-box-info relative col-md-3 col-sm-3 col-xs-6">
							<img src="img/icon_support_white-min.png" alt="Icon Supports" class="absolute">
							<p>support</p>
							<p>life time support 24/7</p>
						</div>
						<div class=" support-box-info relative col-md-3 col-sm-3 col-xs-6">
							<img src="img/icon_patner_white-min.png" alt="Icon partner" class="absolute">
							<p>help partner</p>
							<p>help all aspects</p>
						</div>
						<div class=" support-box-info relative col-md-3 col-sm-3 col-xs-6">
							<img src="img/icon_phone_table_white-min.png" alt="Icon Phone Tablet" class="absolute">
							<p>contact with us</p>
							<p>+07 (0) 7782 9137</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Content Box -->
@endsection