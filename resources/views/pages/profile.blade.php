@extends("layout.app")
@section("content")
    <!-- Content Box -->
	<div class="relative full-width">
		<!-- Breadcrumb -->
		<div class="container-web relative">
			<div class="container">
				<div class="row">
					<div class="breadcrumb-web">
						<ul class="clear-margin">
							<li class="animate-default title-hover-red"><a href="{{route('home')}}">Home</a></li>
							<li class="animate-default title-hover-red"><a href="#">Profile</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- End Breadcrumb -->
		<!-- Content Checkout -->
		<div class="relative container-web">
			<div class="container">
				<div class="row relative">
					<div class="full-width relative top-checkout-box overfollow-hidden top-margin-default">
						{{-- <div class="col-md-6 col-sm-12 col-xs-12 clear-padding-left left-top-checkout">
                            @if (Session::has('messagel'))
                            <div class="alert alert-danger">
                                <ul>
                                    <li>{{Session::get('messagel')}}</li>
                                </ul>
                            </div>
                        @endif
							<div class="full-width box-btn-top-click">
								<p>Sign In</p>
						
								<div class="relative">
									<form method="POST" action="{{route('loginpost')}}" class="form-placeholde-animate">
                                        {{ csrf_field() }}
										<div class="field-wrap">
								            <label>
								            	Email<span class="req">*</span>
								            </label>
								            <input type="text" name="email" required autocomplete="off" />
							            </div>
							            <div class="field-wrap">
								            <label>
								            	Password<span class="req">*</span>
								            </label>
								            <input type="password" name="password" required autocomplete="off" />
							            </div>
							            <div class="relative justify-content form-login-checkout">
							            	<button type="submit" class="animate-default button-hover-red">LOGIN</button>
							            	<ul class="check-box-custom list-radius clear-margin clear-margin">
												<li>
													<label class="clear-margin">Remember me
														<input type="checkbox" >
					  									<span class="checkmark"></span>
					  								</label>
												</li>
											</ul>
							            	<a href="{{route('forgetpassword')}}" class="animate-default title-hover-red">Lost your password?</a>
							            </div>
									</form>
								</div>
							</div>
						</div> --}}

						<div class="col-md-8 col-sm-12 col-xs-12 clear-padding-right right-top-checkout">
                            @if (Session::has('message'))
    <div class="alert alert-danger">
        <ul>
            <li>{{Session::get('message')}}</li>
        </ul>
    </div>
@endif

@if (Session::has('success'))
<div class="alert alert-success">
    <ul>
        <li>{{Session::get('success')}}</li>
    </ul>
</div>
@endif
							<div class="full-width box-btn-top-click">
								<p>Profile Details</p>
							</div>
                            <div class="relative">
                                <form method="POST" action="{{route('updateprofile')}}" class="form-placeholde-animate">
                                    {{ csrf_field() }}
                                    <?php
                                    foreach($customer as $c)
                                    {
                                    ?>
                                    <div class="field-wrap">
                                        <label>
                                            Email<span class="req">*</span>
                                        </label>
                                        <input type="text" value="{{$c->email}}" name="email" required autocomplete="off" />
                                    </div>
                                 
                                    <div class="field-wrap">
                                        <label>
                                            Name<span class="req">*</span>
                                        </label>
                                        <input type="text" value="{{$c->name}}" name="name" required autocomplete="off" />
                                    </div>
                                    <div class="field-wrap">
                                        <label>
                                            Company name(Optional)<span class="req"></span>
                                        </label>
                                        <input type="text" value="{{$c->company_name}}" name="company"  autocomplete="off" />
                                    </div>
                                    <div class="field-wrap">
                                        <label>
                                            Address<span class="req">*</span>
                                        </label>
                                        <input type="text" value="{{$c->address}}" name="address" required autocomplete="off" />
                                    </div>
                                    <div class="field-wrap">
                                        <label>
                                            Phone<span class="req">*</span>
                                        </label>
                                        <input type="text" value="{{$c->phone_number}}" name="phone" required autocomplete="off" />
                                    </div>
                                    <div class="field-wrap">
                                        <label>
                                            Country<span class="req">*</span>
                                        </label>
                                        <input type="text" name="country" value="{{$c->country}}" required autocomplete="off" />
                                    </div>
                                    <div class="field-wrap">
                                        <label>
                                            City/Town<span class="req">*</span>
                                        </label>
                                        <input type="text" name="city" value="{{$c->city}}" required autocomplete="off" />
                                    </div>
                                    <div class="field-wrap">
                                        <label>
                                            Postal code<span class="req">*</span>
                                        </label>
                                        <input type="text" name="postal" value="{{$c->postal_code}}" required autocomplete="off" />
                                    </div>
                                    <div class="field-wrap">
                                        <label>
                                            Password<span class="req">*</span>
                                        </label>
                                        <input type="text" name="password"  autocomplete="off" />
                                    </div>
                                  
                                    <div class="relative justify-content form-login-checkout">
                                        <button type="submit" class="animate-default button-hover-red">Update</button>
                                 </div>
                                 <?php
                                    }
                                 ?>
                                </form>
                            </div>
					
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<!-- End Content Checkout -->
	
	</div>
	<!-- End Content Box -->
@endsection