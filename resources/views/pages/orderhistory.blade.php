@extends("layout.app")
@section("content")
    <!-- Content Box -->
	<div class="relative full-width">
		<!-- Breadcrumb -->
		<div class="container-web relative">
			<div class="container">
				<div class="row">
					<div class="breadcrumb-web">
						<ul class="clear-margin">
							<li class="animate-default title-hover-red"><a href="{{route('home')}}">Home</a></li>
							<li class="animate-default title-hover-red"><a href="#">Order history</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- End Breadcrumb -->
		<!-- Content Checkout -->
		<div class="relative container-web">
			<div class="container">
				<div class="row relative">
					<div class="full-width relative top-checkout-box overfollow-hidden top-margin-default">
						<div class="col-md-12 col-sm-12 col-xs-12 clear-padding-left left-top-checkout">
                         
							<div class="full-width box-btn-top-click">
								<p>Order history</p>
						
                                <br>
                                <br>

                                <table class="table">
                                    <thead class="thead-dark">
                                      <tr>
                                        <th scope="col">Orderid</th>
                                        <th scope="col">Total qty</th>
                                        <th scope="col">Grand total</th>
                                        <th>Status</th>
                                        <th></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                  
                                        <?php
                                        foreach($records as $rr)
                                        {    
                                            if($rr->sale_status==0)
                                            {
                                                $status="Pending";
                                            }
                                            else {
                                                $status="Delivered";
                                            }
                                        ?>
                                        <tr>
                                        <th scope="row">{{$rr->id}}</th>
                                        
                                        <td>{{$rr->total_qty}}</td>
                                        <td>{{$rr->grand_total}}</td>
                                        <td>{{$status}}</td>  
                                        <td><a href="{{route('order_details',$rr->id)}}" type="button" class="btn btn-primary">Details</a></td>
                                    </tr>
                                  <?php
                                        }
                                  ?>
                                    </tbody>
                                  </table>

<br><br><br>
							</div>
						</div>
			
					</div>
					
				</div>
			</div>
		</div>
		<!-- End Content Checkout -->
	
	</div>
	<!-- End Content Box -->
@endsection