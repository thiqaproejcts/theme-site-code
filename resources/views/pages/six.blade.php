@extends("layout.app")
@section("content")
    <!-- Content Box -->
	<div class="relative full-width">
		<!-- Breadcrumb -->
		<div class="container-web relative">
			<div class="container">
				<div class="row">
					<div class="breadcrumb-web">
						<ul class="clear-margin">
							<li class="animate-default title-hover-red"><a href="{{route('home')}}">Home</a></li>
							<li class="animate-default title-hover-red"><a href="#">Six digit code</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- End Breadcrumb -->
		<!-- Content Checkout -->
		<div class="relative container-web">
			<div class="container">
				<div class="row relative">
					<div class="full-width relative top-checkout-box overfollow-hidden top-margin-default">
						<div class="col-md-6 col-sm-12 col-xs-12 clear-padding-left left-top-checkout">
                            @if (Session::has('message'))
                            <div class="alert alert-danger">
                                <ul>
                                    <li>{{Session::get('message')}}</li>
                                </ul>
                            </div>
                        @endif
                        <div class="alert alert-success">
                            <ul>
                                <li>A six digit code is sent on your mail. If you don't find it, check your Junk/Spam foder.</li>
                            </ul>
                        </div>
							<div class="full-width box-btn-top-click">
								<p>Six digit code</p>
						
								<div class="relative">
									<form method="POST" action="{{route('sixdigitpost')}}" class="form-placeholde-animate">
                                        {{ csrf_field() }}
										<div class="field-wrap">
								            <label>
								            	Enter your six digit code<span class="req">*</span>
								            </label>
                                            <input type="text" name="id" value="{{$id}}">
								            <input type="text" name="six" required autocomplete="off" />
							            </div>
							            {{-- <div class="field-wrap">
								            <label>
								            	Password<span class="req">*</span>
								            </label>
								            <input type="password" name="password" required autocomplete="off" />
							            </div> --}}
							            {{-- <div class="relative justify-content form-login-checkout">
							            	<button type="submit" class="animate-default button-hover-red">LOGIN</button>
							            	<ul class="check-box-custom list-radius clear-margin clear-margin">
												<li>
													<label class="clear-margin">Remember me
														<input type="checkbox" >
					  									<span class="checkmark"></span>
					  								</label>
												</li>
											</ul>
							            	<a href="#" class="animate-default title-hover-red">Lost your password?</a>
							            </div> --}}
									</form>
								</div>
							</div>
						</div>
			
					</div>
					
				</div>
			</div>
		</div>
		<!-- End Content Checkout -->
	
	</div>
	<!-- End Content Box -->
@endsection