@extends("layout.app")
@section("content")
    <!-- Content Box -->
	<div class="relative full-width">
		<!-- Breadcrumb -->
		<div class="container-web relative">
			<div class="container">
				<div class="row">
					<div class="breadcrumb-web">
						<ul class="clear-margin">
							<li class="animate-default title-hover-red"><a href="{{route('home')}}">Home</a></li>
							<li class="animate-default title-hover-red"><a href="#">Order history</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- End Breadcrumb -->
		<!-- Content Checkout -->
		<div class="relative container-web">
			<div class="container">
				<div class="row relative">
					<div class="full-width relative top-checkout-box overfollow-hidden top-margin-default">
						<div class="col-md-12 col-sm-12 col-xs-12 clear-padding-left left-top-checkout">
                         
							<div class="full-width box-btn-top-click">
								<p>Order Details</p>
						
                                <br>
                                <br>
								<br>
                                <br>

                                <table class="table">
                                    <thead class="thead-dark">
                                      <tr>
                                        <th scope="col">Sno</th>
                                        <th scope="col">Item name</th>
                                        <th scope="col">Quantity</th>
										<th>Price</th>
                                        <th>Amount</th>
									</tr>
                                    </thead>
                                    <tbody>
                                  
                                        <?php
										$sno=1;
										$total=0;

                                        foreach($records as $rr)
                                        {
											$total+=$rr->total;    
                                        ?>
                                        <tr>
                                        <th scope="row">{{$sno}}</th>
                                        <td>{{$rr->name}}</td>
                                        <td>{{$rr->qty}}</td>
                                        <td>{{$rr->net_unit_price}}</td>
										<td>{{$rr->total}}</td>  
                                    </tr>
                                  <?php
								  $sno++;
                                        }
                                  ?>
								    <tr>
                                        <th scope="row"></th>
                                        <td></td>
                                        <td></td>
                                        <td><b>Subtotal</b></td>
										<td>{{$total}} OMR</td>  
                                    </tr>
									<tr>
                                        <th scope="row"></th>
                                        <td></td>
                                        <td></td>
                                        <td> <b>Shipping Charges</b> </td>
										<td>2 OMR</td>  
                                    </tr>
									<tr>
                                        <th scope="row"></th>
                                        <td>  </td>
                                        <td></td>
                                        <td> <b>Grand Total</b> </td>
										<td>{{$total+2}} OMR</td>  
                                    </tr>
                                    </tbody>
                                  </table>

<br><br><br>
							</div>
						</div>
			
					</div>
					
				</div>
			</div>
		</div>
		<!-- End Content Checkout -->
	
	</div>
	<!-- End Content Box -->
@endsection