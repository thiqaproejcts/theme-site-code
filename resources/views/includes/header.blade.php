<header class="relative full-width box-shadow">
    <div class="clearfix container-web relative">
        <div class=" container">
            <div class="row">
                <div class=" header-top">
                    <p class="contact_us_header col-md-4 col-xs-12 col-sm-3 clear-margin">
                        <img src="{{asset('img/icon_phone_top.png')}}" alt="Icon Phone Top Header" /> Call us <span class="text-red bold"><?php echo $_SESSION["phone"];?></span>
                    </p>
                    <div class="clear-padding menu-header-top text-right col-md-8 col-xs-12 col-sm-6">
                        <ul class="clear-margin">
                            <?php
                            if(isset($_SESSION["userid"]))
                            {
                            ?>

{{-- <li class="relative"><a href="{{route('login')}}">My Account</a></li>
<li class="relative"><a href="{{route('register')}}">Wishlist</a></li> --}}
  <li class="relative">
<a href="#">My Account</a>
<ul>
    <li class="relative"><a href="{{route('profile')}}">Profile</a></li>
    <li class="relative"><a href="{{route('orderhistory')}}">History</a></li>
    {{-- <li class="relative"><a href="#">CN</a></li> --}}
</ul>
</li>
{{-- <li class="relative">
<a href="#">USD</a>
<ul>
    <li class="relative"><a href="#">AUD</a></li>
    <li class="relative"><a href="#">USD</a></li>
    <li class="relative"><a href="#">CAD</a></li>
</ul>
</li> --}}

                            
                            <?php
                                    }
                                    else {
                                ?>
                              <li class="relative"><a href="{{route('login')}}">Login/Register </a></li>
                              {{-- <li class="relative"><a href="{{route('register')}}">Register</a></li> --}}
                                <?php
                                    }
                                ?>
                          
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="clearfix header-content full-width relative">
                    <div class="clearfix icon-menu-bar">
                        <i class="data-icon data-icon-arrows icon-arrows-hamburger-2 icon-pushmenu js-push-menu" aria-hidden="true"></i>
                    </div>
                    <div class="clearfix logo">
                        <?php
                        $lo="../../inventory/public/images/logo/".$_SESSION["logo"];
                        ?>
                        <a href="{{route('home')}}"><img style="width:200px;height:38px;" alt="Logo" src="{{asset($lo)}}" /></a>
                    </div>
                    <div class="clearfix search-box relative float-left">
                        <form method="GET" action="/" class="">
                            <div class="clearfix category-box relative">
                                <select name="cate_search">
                                    <option>All Category</option>
                                    <option>Food</option>
                                    <option>Mobile & Tablet</option>
                                    <option>Electric Appliances</option>
                                    <option>Electronics & Technology</option>
                                    <option>Fashion</option>
                                    <option>Health & Beauty</option>
                                    <option>Mother & Baby</option>
                                    <option>Books & Stationery</option>
                                    <option>Home & Life</option>
                                    <option>Sports & Outdoors</option>
                                    <option>Auto & Moto</option>
                                    <option>Voucher Service</option>
                                </select>
                            </div>
                            <input autocomplete="off" class="itemname" id="search-box" type="text" name="s" placeholder="Enter keyword here . . .">
                            
                            <button type="submit" class="animate-default button-hover-red">Search</button>
                            
                        </form>
                        <div id="suggesstion-box"></div>
                    </div>
                    <div class="clearfix icon-search-mobile absolute">
                        <i onclick="showBoxSearchMobile()" class="data-icon data-icon-basic icon-basic-magnifier"></i>
                    </div>
                    <div class="clearfix cart-website absolute" onclick="showCartBoxDetail()">
                        <img alt="Icon Cart" src="{{asset('img/icon_cart.png')}}" />
                        <p class="count-total-shopping absolute itemcount"></p>
                    </div>
                    <div class="cart-detail-header border">
                        <div class="relative modalcontent">
                         
                          
                        </div>
                        <div class="relative border no-border-l no-border-r total-cart-header">
                            <p class="bold clear-margin">Subtotal:</p>
                            <p class=" clear-margin bold" id="totalamount"></p>
                        </div>
                        <div class="relative btn-cart-header">
                            <a href="{{route('cart')}}" class="uppercase bold animate-default">view cart</a>
                            <a href="{{route('checkout')}}" class="uppercase bold button-hover-red animate-default">checkout</a>
                        </div>
                    </div>
                    <div class="mask-search absolute clearfix" onclick="hiddenBoxSearchMobile()"></div>
                    <div class="clearfix box-search-mobile">
                    </div>
                </div>
            </div>
            <div class="row">
                <a class="menu-vertical hidden-md hidden-lg" onclick="showMenuMobie()">
                    <span class="animate-default"><i class="fa fa-list" aria-hidden="true"></i> all categories</span>
                </a>
            </div>
        </div>
    </div>
    <?php
    //theme3 if condition open
    if($_SESSION["theme"]==3)
    {
    ?>
    <div class="menu-header-v3 hidden-ipx">
        <div class="container">
            <div class="row">
                <!-- Menu Page -->
                <div class="menu-header full-width">
                    <ul class="clear-margin">
                        <li onclick="showMenuHomeV3()"><a class="animate-default" href="#"><i class="fa fa-list" aria-hidden="true"></i> all categories</a></li>
                        <li class="title-hover-red">
                            <a class="animate-default" href="#">home</a>
                            <ul class="sub-menu mega-menu">
                                <li class="relative">
                                    <a class="animate-default center-vertical-image" href="home_v1.html"><img src="img/home_1_menu-min.png" alt=""></a>
                                    <p><a href="home_v1.html">Home 1</a></p>
                                </li>
                                <li class="relative">
                                    <a class="animate-default center-vertical-image" href="home_v2.html"><img src="img/home_2_menu-min.png" alt=""></a>
                                    <p><a href="home_v2.html">Home 2</a></p>
                                </li>
                                <li class="relative">
                                    <a class="animate-default center-vertical-image" href="home_v3.html"><img src="img/home_3_menu-min.png" alt=""></a>
                                    <p><a href="home_v3.html">Home 3</a></p>
                                </li>
                            </ul>
                        </li>
                        <li class="title-hover-red">
                            <a class="animate-default" href="#">shop</a>
                            <div class="sub-menu mega-menu-v2">
                                <ul>
                                    <li>Catgory Type</li>
                                    <li class="title-hover-red"><a class="animate-default clear-padding" href="category_v1.html">Category v1</a></li>
                                    <li class="title-hover-red"><a class="animate-default clear-padding" href="category_v1_2.html">Category v1.2</a></li>
                                    <li class="title-hover-red"><a class="animate-default clear-padding" href="category_v2.html">Category v2</a></li>
                                    <li class="title-hover-red"><a class="animate-default clear-padding" href="category_v2_2.html">Category v2.2</a></li>
                                    <li class="title-hover-red"><a class="animate-default clear-padding" href="category_v3.html">Category v3</a></li>
                                    <li class="title-hover-red"><a class="animate-default clear-padding" href="category_v3_2.html">Category v3.2</a></li>
                                    <li class="title-hover-red"><a class="animate-default clear-padding" href="category_v4.html">Category v4</a></li>
                                    <li class="title-hover-red"><a class="animate-default clear-padding" href="category_v4_2.html">Category v4.2</a></li>
                                </ul>
                                <ul>
                                    <li>Single Product Type</li>
                                    <li class="title-hover-red"><a class="animate-default clear-padding" href="product_v1.html">Product Single 1</a></li>
                                    <li class="title-hover-red"><a class="animate-default clear-padding" href="product_v2.html">Product Single 2</a></li>
                                    <li class="title-hover-red"><a class="animate-default clear-padding" href="product_v3.html">Product Single 3</a></li>
                                </ul>
                                <ul>
                                    <li>Order Page</li>
                                    <li class="title-hover-red"><a class="animate-default clear-padding" href="cartpage.html">Cart Page</a></li>
                                    <li class="title-hover-red"><a class="animate-default clear-padding" href="checkout.html">Checkout</a></li>
                                    <li class="title-hover-red"><a class="animate-default clear-padding" href="compare.html">Compare</a></li>
                                    <li class="title-hover-red"><a class="animate-default clear-padding" href="quickview.html">QuickView</a></li>
                                    <li class="title-hover-red"><a class="animate-default clear-padding" href="trackyourorder.html">Track Order</a></li>
                                    <li class="title-hover-red"><a class="animate-default clear-padding" href="wishlist.html">WishList</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="title-hover-red">
                            <a class="animate-default" href="#">pages</a>
                            <ul>
                                <li class="title-hover-red"><a class="animate-default" href="about.html">About Us</a></li>
                                <li class="title-hover-red"><a class="animate-default" href="contact.html">Contact</a></li>
                                <li class="title-hover-red"><a class="animate-default" href="404.html">404</a></li>
                            </ul>
                        </li>
                        <li class="title-hover-red">
                            <a class="animate-default" href="#">Blog</a>
                            <ul>
                                <li class="title-hover-red"><a class="animate-default" href="blog.html">Blog Category</a></li>
                                <li class="title-hover-red"><a class="animate-default" href="blogdetail.html">Blog Detail</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- End Menu Page -->
            </div>
        </div>
    </div>
   
    <div class="clearfix menu_more_header menu-web menu-bg-white">
        <ul>
            <?php
            $catt=DB::select(" SELECT * FROM `categories` WHERE `trader_id`='".$_SESSION["trader_id"]."' ");
            foreach($catt as $ct)
            {    
            ?>
            <li><a href="/categorie-items/{{$ct->id}}"><img src="img/icon_hot_gray.png" alt="Icon Hot Deals" /> <p>{{$ct->name}}</p></a></li>
                <?php
            }
                ?>
            {{-- <li><a href="#"><img src="img/icon_food_gray.png" alt="Icon Food" /> <p>Food</p></a></li>
            <li><a href="#"><img src="img/icon_mobile_gray.png" alt="Icon Mobile & Tablet" /> <p>Mobile & Tablet</p></a></li>
            <li><a href="#"><img src="img/icon_electric_gray.png" alt="Icon Electric Appliances" /> <p>Electric Appliances</p></a></li>
            <li><a href="#"><img src="img/icon_computer_gray.png" alt="Icon Electronics & Technology" /> <p>Electronics & Technology</p></a></li>
            <li><a href="#"><img src="img/icon_fashion_gray.png" alt="Icon Fashion" /> <p>Fashion</p></a></li>
            <li><a href="#"><img src="img/icon_health_gray.png" alt="Icon Health & Beauty" /> <p>Health & Beauty</p></a></li>
            <li><a href="#"><img src="img/icon_mother_gray.png" alt="Icon Mother & Baby" /> <p>Mother & Baby</p></a></li>
            <li><a href="#"><img src="img/icon_book_gray.png" alt="Icon Books & Stationery" /> <p>Books & Stationery</p></a></li>
            <li><a href="#"><img src="img/icon_home_gray.png" alt="Icon Home & Life" /> <p>Home & Life</p></a></li>
            <li><a href="#"><img src="img/icon_sport_gray.png" alt="Icon Sports & Outdoors" /> <p>Sports & Outdoors</p></a></li>
            <li><a href="#"><img src="img/icon_auto_gray.png" alt="Icon Auto & Moto" /> <p>Auto & Moto</p></a></li>
            <li><a href="#"><img src="img/icon_voucher_gray.png" alt="Icon Voucher Service" /> <p>Voucher Service</p></a></li> --}}
        </ul>
    </div>

    <?php
}
?>
            <div class="header-ontop">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="clearfix logo">
                        <?php
                        $lo="../../inventory/public/images/logo/".$_SESSION["logo"];
                        ?>
                        <a href="{{route('home')}}"><img style="width:200px;height:38px;" alt="Logo" src="{{asset($lo)}}" /></a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="menu-header">
                        <ul class="main__menu clear-margin">
                            <li class="title-hover-red">
                                <a class="animate-default" href="{{route('home')}}">Home</a>
                                {{-- <ul class="sub-menu mega-menu">
                                    <li class="relative">
                                        <a class="animate-default center-vertical-image" href="home_v1.html"><img src="{{asset('img/home_1_menu-min.png')}}" alt=""></a>
                                        <p><a href="home_v1.html">Home 1</a></p>
                                    </li>
                                    <li class="relative">
                                        <a class="animate-default center-vertical-image" href="home_v2.html"><img src="{{asset('img/home_2_menu-min.png')}}" alt=""></a>
                                        <p><a href="home_v2.html">Home 2</a></p>
                                    </li>
                                    <li class="relative">
                                        <a class="animate-default center-vertical-image" href="home_v3.html"><img src="{{asset('img/home_3_menu-min.png')}}" alt=""></a>
                                        <p><a href="home_v3.html">Home 3</a></p>
                                    </li>
                                </ul> --}}
                            </li>
                            <li class="title-hover-red">
                                <a class="animate-default" href="{{route('contact')}}">Contact us</a>
                                {{-- <ul class="sub-menu mega-menu">
                                    <li class="relative">
                                        <a class="animate-default center-vertical-image" href="home_v1.html"><img src="{{asset('img/home_1_menu-min.png')}}" alt=""></a>
                                        <p><a href="home_v1.html">Home 1</a></p>
                                    </li>
                                    <li class="relative">
                                        <a class="animate-default center-vertical-image" href="home_v2.html"><img src="{{asset('img/home_2_menu-min.png')}}" alt=""></a>
                                        <p><a href="home_v2.html">Home 2</a></p>
                                    </li>
                                    <li class="relative">
                                        <a class="animate-default center-vertical-image" href="home_v3.html"><img src="{{asset('img/home_3_menu-min.png')}}" alt=""></a>
                                        <p><a href="home_v3.html">Home 3</a></p>
                                    </li>
                                </ul> --}}
                            </li>
                            <li class="title-hover-red">
                                <a class="animate-default" href="{{route('about')}}">About us</a>
                                {{-- <ul class="sub-menu mega-menu">
                                    <li class="relative">
                                        <a class="animate-default center-vertical-image" href="home_v1.html"><img src="{{asset('img/home_1_menu-min.png')}}" alt=""></a>
                                        <p><a href="home_v1.html">Home 1</a></p>
                                    </li>
                                    <li class="relative">
                                        <a class="animate-default center-vertical-image" href="home_v2.html"><img src="{{asset('img/home_2_menu-min.png')}}" alt=""></a>
                                        <p><a href="home_v2.html">Home 2</a></p>
                                    </li>
                                    <li class="relative">
                                        <a class="animate-default center-vertical-image" href="home_v3.html"><img src="{{asset('img/home_3_menu-min.png')}}" alt=""></a>
                                        <p><a href="home_v3.html">Home 3</a></p>
                                    </li>
                                </ul> --}}
                            </li>
                            {{-- <li class="title-hover-red">
                                <a class="animate-default" href="#">shop</a>
                                <div class="sub-menu mega-menu-v2">
                                    <ul>
                                        <li>Catgory Type</li>
                                        <li class="title-hover-red"><a class="animate-default clear-padding" href="category_v1.html">Category v1</a></li>
                                        <li class="title-hover-red"><a class="animate-default clear-padding" href="category_v1_2.html">Category v1.2</a></li>
                                        <li class="title-hover-red"><a class="animate-default clear-padding" href="category_v2.html">Category v2</a></li>
                                        <li class="title-hover-red"><a class="animate-default clear-padding" href="category_v2_2.html">Category v2.2</a></li>
                                        <li class="title-hover-red"><a class="animate-default clear-padding" href="category_v3.html">Category v3</a></li>
                                        <li class="title-hover-red"><a class="animate-default clear-padding" href="category_v3_2.html">Category v3.2</a></li>
                                        <li class="title-hover-red"><a class="animate-default clear-padding" href="category_v4.html">Category v4</a></li>
                                        <li class="title-hover-red"><a class="animate-default clear-padding" href="category_v4_2.html">Category v4.2</a></li>
                                    </ul>
                                    <ul>
                                        <li>Single Product Type</li>
                                        <li class="title-hover-red"><a class="animate-default clear-padding" href="product_v1.html">Product Single 1</a></li>
                                        <li class="title-hover-red"><a class="animate-default clear-padding" href="product_v2.html">Product Single 2</a></li>
                                        <li class="title-hover-red"><a class="animate-default clear-padding" href="product_v3.html">Product Single 3</a></li>
                                    </ul>
                                    <ul>
                                        <li>Order Page</li>
                                        <li class="title-hover-red"><a class="animate-default clear-padding" href="cartpage.html">Cart Page</a></li>
                                        <li class="title-hover-red"><a class="animate-default clear-padding" href="checkout.html">Checkout</a></li>
                                        <li class="title-hover-red"><a class="animate-default clear-padding" href="compare.html">Compare</a></li>
                                        <li class="title-hover-red"><a class="animate-default clear-padding" href="quickview.html">QuickView</a></li>
                                        <li class="title-hover-red"><a class="animate-default clear-padding" href="trackyourorder.html">Track Order</a></li>
                                        <li class="title-hover-red"><a class="animate-default clear-padding" href="wishlist.html">WishList</a></li>
                                    </ul>
                                </div>
                            </li> --}}
                            {{-- <li class="title-hover-red">
                                <a class="animate-default" href="#">pages</a>
                                <ul>
                                    <li class="title-hover-red"><a class="animate-default" href="about.html">About Us</a></li>
                                    <li class="title-hover-red"><a class="animate-default" href="contact.html">Contact</a></li>
                                    <li class="title-hover-red"><a class="animate-default" href="404.html">404</a></li>
                                </ul>
                            </li>
                            <li class="title-hover-red">
                                <a class="animate-default" href="#">Blog</a>
                                <ul>
                                    <li class="title-hover-red"><a class="animate-default" href="blog.html">Blog Category</a></li>
                                    <li class="title-hover-red"><a class="animate-default" href="blogdetail.html">Blog Detail</a></li>
                                </ul>
                            </li> --}}
                        </ul>
                        <div class="clearfix cart-website absolute" onclick="showCartBoxDetail()">
                            <img alt="Icon Cart" src="{{asset('img/icon_cart.png')}}" />
                            <p class="count-total-shopping absolute itemcount"></p>
                        </div>
                        <div class="cart-detail-header border">
                            <div class="relative modalcontent">
                             
                              
                            </div>
                            <div class="relative border no-border-l no-border-r total-cart-header">
                                <p class="bold clear-margin">Subtotal:</p>
                                <p class=" clear-margin bold" id="totalamount"></p>
                            </div>
                            <div class="relative btn-cart-header">
                                <a href="{{route('cart')}}" class="uppercase bold animate-default">view cart</a>
                                <a href="{{route('checkout')}}" class="uppercase bold button-hover-red animate-default">checkout</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- End Header Box -->
