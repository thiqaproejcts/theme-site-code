<?php
//session_start();
// $token="xCfUqVMyiyL5iE2mRD78le3XTL7sAHoBh2xpZqwt";
//         $check=DB::select(" SELECT * FROM `site_configuration` WHERE `token`='".$token."' ");
//         //dd($check);
//         foreach($check as $cc)
//         {
//                $trader_id=$cc->trader_id;
//                $theme=$cc->theme;
//                $phone=$cc->phone;
//                $email=$cc->email;
               
//                $_SESSION["trader_id"]=$trader_id;
//                $_SESSION["phone"]=$phone;
//                $_SESSION["email"]=$email;
//                $_SESSION["theme"]=$theme;
        //}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Theme site</title>
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css/icon-font-linea.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/multirange.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-theme.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/themify-icons.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/effect.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/home.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/contact.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/about.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/cartpage.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/product.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/slick-theme.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/category.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/owl.theme.default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/responsive.css')}}">
    
</head>

<style>
/* The snackbar - position it at the bottom and in the middle of the screen */
#snackbar {
  visibility: hidden; /* Hidden by default. Visible on click */
  min-width: 250px; /* Set a default minimum width */
  margin-left: -125px; /* Divide value of min-width by 2 */
  background-color: #333; /* Black background color */
  color: #fff; /* White text color */
  text-align: center; /* Centered text */
  border-radius: 10px; /* Rounded borders */
  padding: 16px; /* Padding */
  position: fixed; /* Sit on top of the screen */
  z-index: 100; /* Add a z-index if needed */
  left: 50%; /* Center the snackbar */
  bottom: 30px; /* 30px from the bottom */
}

/* Show the snackbar when clicking on a button (class added with JavaScript) */
#snackbar.show {
  visibility: visible; /* Show the snackbar */
  /* Add animation: Take 0.5 seconds to fade in and out the snackbar.
  However, delay the fade out process for 2.5 seconds */
  -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
  animation: fadein 0.5s, fadeout 0.5s 2.5s;
}

/* Animations to fade the snackbar in and out */
@-webkit-keyframes fadein {
  from {bottom: 0; opacity: 0;}
  to {bottom: 30px; opacity: 1;}
}

@keyframes fadein {
  from {bottom: 0; opacity: 0;}
  to {bottom: 30px; opacity: 1;}
}

@-webkit-keyframes fadeout {
  from {bottom: 30px; opacity: 1;}
  to {bottom: 0; opacity: 0;}
}

@keyframes fadeout {
  from {bottom: 30px; opacity: 1;}
  to {bottom: 0; opacity: 0;}
}


    /* search
/* -------------------------------------------------------------------------- */
.search-drop{
    border: #48B35B 2px solid;
    border-radius:5px;
    position: absolute;
    background-color: white;
    width:500px;
    height:500px;
    z-index : 100;
    margin-top: 5px;
    overflow-y:scroll;
}



@media (max-width: 1100px) {
    .search-drop{
      width: 650px;
  }



}



@media (max-width: 992px) {
    .search-drop{
      width: 400px;
  }




@media (max-width: 450px) {
    .search-drop{
      width: 370px;
  }



}
}

@media (max-width: 380px) {
    .search-drop{
      width: 350px;
  }



}

@media (max-width: 311px) {
    .search-drop{
      width: 320px;
  }


}



@media (max-width: 280px) {
    .search-drop{
      width: 250px;
  }


}



.table-image td, .table-image th {
  vertical-align: middle;
  text-align: center;

}
.table-image td.qty, .table-image th.qty {
  max-width: 2rem;
}
.tableitem:hover {
  /*background-color: #6867AC;*/
}
</style>

<body>
 <!-- The actual snackbar -->
<div id="snackbar">Added to the cart..</div> 
@include('includes/push')
 <div class="wrappage">
@include('includes/header')
@yield('content')
@include('includes/footer')
</div>
<script src="{{asset('js/jquery-3.3.1.min.js')}}" defer=""></script>
<script src="{{asset('js/bootstrap.min.js')}}" defer=""></script>
<script src="{{asset('js/multirange.js')}}" defer=""></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCR8GCPpAFKo4Dke4SsPGcao1NKnn25OQA&v=3.31&callback=initMap" defer=""></script>
<script src="{{asset('js/owl.carousel.min.js')}}" defer=""></script>
<script src="{{asset('js/sync_owl_carousel.js')}}" defer=""></script>
<script src="{{asset('js/scripts.js')}}" defer=""></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
<script>

$(document).ready(function(){


total();
modal();
item();
//saved();

});

    // AJAX call for autocomplete
$(document).ready(function(){
$("#search-box").keyup(function(){
 console.log($(this).val());
key=$(this).val();
  $.ajax({
  type: "GET",
  url: "{{route('searchitem')}}",
  data:
  {
      key:key
  },
 success: function(data){
    // console.log(data);
    if(data=="")
      {
        $("#suggesstion-box").style.display = 'none';
      }
      $("#suggesstion-box").fadeIn();
      $("#suggesstion-box").html(data);
      $("#search-box").css("background","#FFF");
      
  }
  });
});
});


$(document).ready(function(){
$('.addtocart').on('submit',function(e){
    e.preventDefault();
    //var name=$(this).find('.itemname').val();
    //var pprice=$(this).find('.itemprice').val();
    //var sprice=$(this).find('.itemsprice').val();
    //var cprice=$(this).find('.itemcprice').val();
    var qty=$(this).find('#qty').val();
    var itemcode=$(this).find('#itemcode').val();
    //var remarks=$(this).find('.remarks').val();
    //var company=$(this).find('.company').val();
    
//alert(qty+"\n"+itemcode);
     $.ajax({
  url:'{{route('addtocart')}}',
  //headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
  method:'POST',
  data:{
     _token:"{{ csrf_token() }}",
    itemcode:itemcode,
    qty:qty
  },
  success:function(response){

   //console.log(response);
       total();
       modal();
       item();
       myFunction();
  }
});
});
 });

 function modal(){
$.ajax({
  url:'{{route('modelcontent')}}',
  method:'POST',
  data:{
    _token: "{{ csrf_token() }}",
    },
  success:function(data){
      //console.log(data);
$('.modalcontent').html(data);
  }
});
}

function total(){
  $.ajax({
    url:'{{route('total')}}',
    method:'POST',
    data:{
      _token: "{{ csrf_token() }}"
    },
  success:function(data){
      //console.log(data);
        $('#totalamount').html("$"+data);
        $('#totalamountall').html("$"+(Number(data)+20));
        // $('.gross_invoice').val(data[0]);
        // $("#ballance").val(data[3]);
    }
  });
}

function item()
{
  $.ajax({
    url:"{{route('itemcount')}}",
    method:"POST",
    data:
    {
      _token: "{{ csrf_token() }}"
    },
    success:function(data)
    {
      $(".itemcount").html(data);
    }
  });
}

function myFunction() {
  // Get the snackbar DIV
  var x = document.getElementById("snackbar");

  // Add the "show" class to DIV
  x.className = "show";

  // After 3 seconds, remove the show class from DIV
  setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}

</script>
</body>

</html>
