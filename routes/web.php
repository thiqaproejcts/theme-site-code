<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get("/","HomeController@index")->name("home");
Route::get("/mycart","HomeController@mycart")->name("cart");
Route::get("/about-us","HomeController@aboutus")->name("about");
Route::get("/contact-us","HomeController@contactus")->name("contact");
Route::get("/checkout","HomeController@checkout")->name("checkout")->middleware("check.session");
Route::get("/trackorder","HomeController@trackorder")->name("trackorder");
Route::get("/orderhistory","HomeController@orderhistory")->name("orderhistory")->middleware("check.session");;
Route::get("/order-details/{id}","HomeController@orderdetails")->name("order_details")->middleware("check.session");;
Route::get("/terms-and-conditions","HomeController@terms")->name("terms");
Route::get("/privacy-policy","HomeController@privacy")->name("privacy");
Route::get("/product-details/{id}","HomeController@productdetails")->name("productdetails");
Route::get("/wishlist","HomeController@wishlist")->name("wishlist");
Route::get("/login","HomeController@login")->name("login");
Route::get("/success","HomeController@success_page")->name("success");
//Checkout Controller
Route::post("/checkout","CheckoutController@checkoutfinal")->name("checkout.final");

//Ajax requests
Route::get("/search-by-itemname","AjaxController@searchbyitemname")->name("searchitem");
Route::get("/remove/{id}","AjaxController@remove")->name("remove");

Route::post("/addtocart","AjaxController@addtocart")->name("addtocart");
Route::post("/modelcontent","AjaxController@modelcontent")->name("modelcontent");
Route::post("/totalcart","AjaxController@total")->name("total");
Route::post("/itemcount","AjaxController@itemcount")->name("itemcount");
//Route::post("/login","AjaxController@loginpost")->name("loginpost");

//Register Controller
Route::post("/register","RegisterController@register")->name("register");
Route::post("/login-post","LoginController@loginpost")->name("loginpost");

//Forget Controller
Route::get("/forget-password","ForgetController@forgetPassword")->name("forgetpassword");
Route::post("/six-digit-code","ForgetController@sixDigit")->name("enteremail");
Route::post("/check-six-digit-code","ForgetController@checksixdigit")->name("sixdigitpost");
Route::post("/set-new-password","ForgetController@setNewPassword")->name("setnew");
Route::post("/set-password-post","ForgetController@setPassword")->name("setpasswordpost");

//Profile routes
Route::get("/profile","ProfileController@profile")->name("profile")->middleware("check.session");;
Route::post("/update-profile","ProfileController@updateprofile")->name("updateprofile")->middleware("check.session");;

//Category routes
Route::get("/category/{id}","CategoryController@index")->name("category");